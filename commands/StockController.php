<?php

namespace app\commands;

use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\db\Expression as DbExpression;
use yii\db\Query;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use app\models\Stock;
use app\models\StockChart;
use app\models\StockPrice;
use app\models\StockGrowthDirection;
use app\models\StockRoi;

/**
 * Class StockController
 * @package app\commands
 */
class StockController extends \yii\console\Controller
{
    public $exchanges = [
        'NASDAQ', 'NYSE'
    ];

    public function init()
    {
        parent::init();
        if (!function_exists('curl_init')) {
            throw new ErrorException('The class depends on curl but curl not found in the extensions list');
        }
    }

    /**
     * Full update
     *
     * @return boolean
     */
    public function actionFullUpdate()
    {
        return $this->actionDownloadStocksDataAndLoadExchangesData()
            && $this->actionDownloadStockPricesAndProcessStockPricesAndUpdateCharts()
            && $this->actionUpdateEps()
            && $this->actionCalculateGrowthDirection();

    }

    /**
     * Downloads stock prices and processes stock prices
     * and updates charts
     *
     * @return boolean
     */
    public function actionDownloadStockPricesAndProcessStockPricesAndUpdateCharts()
    {
        return $this->actionDownloadStockPrices() 
            && $this->actionProcessStockPrices()
            && $this->actionUpdateCharts();
    }

    /**
     * Downloads csv file from NASDAQ`s site and loads exchanges data
     *
     * @return boolean
     */
    public function actionDownloadStocksDataAndLoadExchangesData()
    {
        return $this->actionDownloadStocksData() && $this->actionLoadExchangesData();
    }

    /**
     * Updates EPS values
     *
     * @return boolean
     */
    public function actionUpdateEps()
    {

        $limit = 50;
        $offset = 0;
        $total = Stock::find()->count();
        $curlHandle = curl_init();

        $this->stdout('Need to process ' . $total . ' stocks...' . PHP_EOL);

        do {

            $stocks = Stock::find()
                ->offset($offset)
                ->limit($limit)
                ->all();

            $stocksCount = count($stocks);
            for ($i = 0; $i < $stocksCount; ++$i) {

                $stock = $stocks[$i];

                $url = 'http://ca.finance.yahoo.com/d/quotes.csv?f=e&e=.csv&s=' . $stock->symbol;
                $this->stdout('Downloading EPS value for "' . $stock->symbol . '", url: ' . $url . PHP_EOL);
                $this->prepareCurl($curlHandle, $url);

                $response = curl_exec($curlHandle);
                $curlErrNo = curl_errno($curlHandle);
                if ($curlErrNo != CURLE_OK) {
                    $this->stderr(
                        'Can not download EPS data, message: ' . curl_error($curlHandle) . '. Try again...' . PHP_EOL
                    );
                    --$i;
                    continue;
                }

                $epsValue = trim($response);
                if (!$epsValue) {
                    $this->stderr('Empty EPS value!' . PHP_EOL);
                    continue;
                } else {
                    $this->stdout('Server response: ' . $epsValue . PHP_EOL);
                    if (!is_numeric($epsValue)) {
                        $this->stderr('EPS value is not numeric!' . PHP_EOL);
                        continue;
                    }
                }

                // Get newest stock roi for the stock
                $stockRoiModel = StockRoi::find()
                    ->where(['stock_id' => $stock->id])
                    ->orderBy('date desc')
                    ->one();
                if (!$stockRoiModel) {
                    
                    $stockRoiModel = new StockRoi();
                    $stockRoiModel->date = date('Y-m-01');
                    $stockRoiModel->stock_id = $stock->id;

                    $this->stdout('A new stock roi model will be save' . PHP_EOL);
                }

                $stockRoiModel->eps = $epsValue;
                if (!$stockRoiModel->validate()) {
                    $this->stderr('Can not validate stock roi model data, errors: ' . json_encode($stock->errors) . PHP_EOL);
                    curl_close($curlHandle);
                    return false;
                } 
                if (!$stockRoiModel->save()) {
                    $this->stderr('Can not save stock roi model, errors: ' . json_encode($stock->errors) . PHP_EOL);
                    curl_close($curlHandle);
                    return false;
                }

                $this->stdout('Success!' . PHP_EOL);
            }    

            $offset += $limit;

        } while ($offset + $limit < $total);

        curl_close($curlHandle);

        $this->stdout('Work is done!' . PHP_EOL);

        return true;
    }
    
    /**
     * Updates chart images
     */
    public function actionUpdateCharts()
    {
        $this->stdout('Updating chart images...' . PHP_EOL);
        
        // Get total count of stocks
        $totalCount = Stock::find()->count();
        if ($totalCount == 0) {
            $this->stderr('No stocks found in database' . PHP_EOL);
            return false;
        }
        
        // Initialize vars
        $limit = 50;
        $offset = 0;
        $curlHandle = curl_init();
        
        // Process stocks page after page, fifteen items on a page
        do {
            
            $list = Stock::find()
                ->offset($offset)
                ->limit($limit)
                ->all();
            
            $cnt = count($list);    
            for ($i = 0; $i < $cnt; ++$i) {
                /** @var Stock $item */
                $item = $list[$i];
                
                $this->stdout('Processing stock "' . $item->symbol . '"...' . PHP_EOL);
                
                // For each stock chart type do
                $types = array_keys(StockChart::$types);
                $stCnt = count($types);
                for ($j = 0; $j < $stCnt; $j++) {

                    $type = StockChart::$types[$types[$j]];

                    // Instantiate StockChart instance
                    $stockChart = new StockChart($item->symbol, $type);
                    if (!$stockChart->shouldUpdate()) {
                        $this->stdout('Chart image for the stock exists and actual.' . PHP_EOL);
                        continue;
                    }
                    $this->stdout('Should download chart image...' . PHP_EOL);

                    try {
                        // Get destination path and check it
                        $destinationPath = $stockChart->getChartFilePath();
                        $destinationDirPath = dirname($destinationPath);
                        $this->stdout('Chart image will be written to "' . $destinationPath . '"' . PHP_EOL);
                        if (!file_exists($destinationDirPath)) {
                            if (!FileHelper::createDirectory($destinationDirPath)) {
                                throw new Exception('Can not create directory: ' . $destinationDirPath . '!' . PHP_EOL);
                            }
                        }
                        if (!is_writable($destinationDirPath)) {
                            throw new Exception('Can not write to "' . $destinationDirPath . '"!' . PHP_EOL);
                        }           
                        unset($destinationDirPath);     
                        
                        // Download chart image from source url
                        $sourceUrl = $stockChart->getSourceUrl();
                        $this->prepareCurl($curlHandle, $stockChart->getSourceUrl());
                        $this->stdout('Downloading from "' . $sourceUrl . '"...' . PHP_EOL);
                        $response = curl_exec($curlHandle);
                        // Check for errors
                        if (curl_errno($curlHandle) != CURLE_OK) {
                            $this->stderr(
                                'Can not download chart image, curl error #' . curl_errno($curlHandle)
                                    . ', message: ' . curl_error($curlHandle) . PHP_EOL
                            );
                            $this->stderr('Try again...' . PHP_EOL);
                            --$j;
                            continue;
                        }
                        // HTTP status code should be equals to 200
                        $httpStatusCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
                        if ($httpStatusCode != 200) {
                            $this->stderr('Can not download chart image, http status code: ' . $httpStatusCode . PHP_EOL);
                            continue;
                        }

                    } catch (\Exception $e) {
                        $this->stderr($e->getMessage() . PHP_EOL);
                        curl_close($curlHandle);
                        return false;
                    }
                    
                    // Save file to disc
                    file_put_contents($destinationPath, $response);
                    
                    $this->stdout('Success!' . PHP_EOL);
                }
            }    
            
            $offset += $limit;
            
        } while ($offset + $limit < $totalCount);
        
        curl_close($curlHandle);
        
        $this->stdout('Work is done!' . PHP_EOL);

        return true;
    }

    /**
     * Processes stock prices
     * Calculates ROI1, ROI2, ROI3, ROI3m, ROI3years
     * Sets current stock price
     *
     * @param int $countOfExistsRecordsToBreak 
     * @return boolean
     *
     */
    public function actionProcessStockPrices($countOfExistsRecordsToBreak = 10)
    {
        $this->stdout('Processing stock prices...' . PHP_EOL);

        // Get date before a month of last price date
        $result = (new Query())
            ->select('date')
            ->from('stock_price')
            ->offset(1)
            ->orderBy('date asc')
            ->limit(1)
            ->one();
        if (!$result) {
            $this->stderr('A date before a month of last price date is not found' . PHP_EOL);
            return false;
        }    
        $dateBeforeAMonthOfLastPriceDateDt = new \DateTime($result['date']);
        $this->stdout('A date before last a month of last price: ' . $result['date'] . PHP_EOL);
        unset($result);    

        // Iterate over each stock
        $offset = 0;
        $limit = 50;
        $oneMonthInterval = \DateInterval::createFromDateString('+1 months');
        $total = Stock::find()->count();
        if (!$total) {
            $this->stderr('No stocks in a database' . PHP_EOL);
            return false;
        }
        do {

            $stocks = Stock::find()
                ->offset($offset)
                ->limit($limit)
                ->all();

            foreach ($stocks as $stock) {

                $this->stdout("Processing stock \"{$stock->name}\"...\n");

                // Iterate over each month from current date to date before a month of last price 
                $currentDt = new \DateTime('first day of this month');
                $rows = [];
                $countOfRecordsExists = 0;
                do {

                    $currentDate = $currentDt->format('Y-m-d');

                    $this->stdout("Processing date \"$currentDate\"...\n");

                    // Check data already exists
                    $exists = StockRoi::find()
                        ->where(['stock_id' => $stock->id])
                        ->andWhere(['date' => $currentDate])
                        ->exists();
                    if ($exists) {
                        $this->stdout(
                            'Data already exists for stock "' . $stock->symbol 
                                . '" for a date "' . $currentDate . '"!' . PHP_EOL
                        );
                        $currentDt->sub($oneMonthInterval);

                        $countOfRecordsExists++;
                        if ($countOfRecordsExists == $countOfExistsRecordsToBreak) {
                            $this->stdout('Max count of exists record reached, break update.' . PHP_EOL);
                            break;
                        }

                        continue;
                    } else {
                        $countOfRecordsExists = 0;
                    }

                    // Create new stock roi model
                    $stockRoiModel = new StockRoi();
                    $stockRoiModel->stock_id = $stock->id;
                    $stockRoiModel->date = $currentDate;
                    if (!$stockRoiModel->validate()) {
                        $this->stderr('Can not validate data, errors: ' . json_encode($stockRoiModel->errors) . PHP_EOL);
                        return false;    
                    }

                    // Show info
                    $this->stdout('Symbol: ' . $stock->symbol . PHP_EOL);
                    $this->stdout('Date: ' . $currentDate . PHP_EOL);
                    $this->stdout('ROI1: ' . $stockRoiModel->roi_1 . PHP_EOL);
                    $this->stdout('ROI2: ' . $stockRoiModel->roi_2 . PHP_EOL);
                    $this->stdout('ROI3: ' . $stockRoiModel->roi_3 . PHP_EOL);
                    $this->stdout('ROI1M: ' . $stockRoiModel->roi_1m . PHP_EOL);
                    $this->stdout('ROI3M: ' . $stockRoiModel->roi_3m . PHP_EOL);
                    $this->stdout('ROI6M: ' . $stockRoiModel->roi_6m . PHP_EOL);
                    $this->stdout('ROI3Y: ' . $stockRoiModel->roi_3years . PHP_EOL);
                    $this->stdout('ROI BEST: ' . $stockRoiModel->roi_best . PHP_EOL);
                    $this->stdout('ROI WORST: ' . $stockRoiModel->roi_worst . PHP_EOL);

                    // If ROI1M is null, then no reason to continue for the stock
                    if (!$stockRoiModel->roi_1m) {
                        $this->stdout('ROI1M is empty, no reason to continue for the stock...' . PHP_EOL);
                        break;
                    }

                    $rows[] = $stockRoiModel->attributes;

                    $currentDt->sub($oneMonthInterval);
                } while ($currentDt > $dateBeforeAMonthOfLastPriceDateDt);

                // Save data
                if ($rows) {
                    try {
                        Yii::$app->db->createCommand()
                            ->batchInsert(StockRoi::tableName(), $stockRoiModel->attributes(), $rows)
                            ->execute();
                        $this->stdout('Data successfully saved.' . PHP_EOL);
                    } catch (\Exception $e) {
                        $this->stderr('Can not save data, reason: ' . $e->getMessage() . PHP_EOL);
                        return false;
                    }
                }

                // Set current stock price and volume
                $spModel = StockPrice::getStockPriceModelByStockAndDateTimeLive($stock, new \DateTime());
                if (!$spModel) {
                    $this->stdout('Can not update current stock price and volume, price model not found!' . PHP_EOL);
                    continue;
                }

                $stock->stock_price = $spModel->open;
                $stock->volume = $spModel->volume;
                if (!$stock->validate()) {
                    $this->stderr('Can not validate stock data, errors: ' . json_encode($stock->errors) . PHP_EOL);
                    continue;
                }
                if (!$stock->save()) {
                    $this->stderr('Can not save stock, errors: ' . json_encode($stock->errors) . PHP_EOL);
                    continue;
                }
                $this->stdout('Stock price and volume are updated...' . PHP_EOL);
            }    

            $offset += $limit;

        } while ($offset + $limit < $total);        

        $this->stdout('Prices processed.' . PHP_EOL);

        return true;
    }

    /**
     * Downloads stock prices
     *
     * @param boolean $fullUpdate If flag is up then the code will try to update all records, else
     * just new records will be added.
     * @param int $countOfExistsRecordToBreakUpdate Count of records that exists (found in the db) to break update process
     *
     * @return boolean
     */
    public function actionDownloadStockPrices($fullUpdate = false, $countOfExistsRecordToBreakUpdate = 10)
    {
        $this->stdout('Downloading stock prices...' . PHP_EOL);

        // Get total count of stocks
        $totalCount = Stock::find()->count();
        if ($totalCount == 0) {
            $this->stderr('No stocks found in database' . PHP_EOL);
            return false;
        }

        // Create new curl handle and initialize vars
        $curlHandle = curl_init();
        $limit = 50;
        $offset = 0;

        do {

            // Get stock models
            $list = Stock::find()
                ->offset($offset)
                ->limit($limit)
                ->all();

            // For each item in the list do...
            $listCount = count($list);
            for ($i = 0; $i < $listCount; ++$i) {

                /** @var $stockModel Stock */
                $stockModel = $list[$i];

                // Download contents from url
                $url = 'http://ichart.yahoo.com/table.csv?g=m&s=' . $stockModel->symbol;
                $this->stdout('Downloading contents from url "' . $url . '"' . PHP_EOL);
                $this->prepareCurl($curlHandle, $url);
                $response = curl_exec($curlHandle);

                // Process possible curl errors
                $errNo = curl_errno($curlHandle);
                if ($errNo != CURLE_OK) {
                    $this->stderr('Curl error #' . curl_errno($curlHandle) . ', message: ' . curl_error($curlHandle) . PHP_EOL);
                    switch ($errNo) {
                        // case CURLE_OPERATION_TIMEOUTED:
                        default:
                            --$i;
                            continue;
                        // default:
                            // curl_close($curlHandle);
                            // return false;
                    }
                }
                $httpStatusCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
                if ($httpStatusCode != 200) {
                    $this->stderr('Invalid HTTP status code: ' . $httpStatusCode . PHP_EOL);
                    continue;
                }

                // Parse downloaded content
                $lines = explode(PHP_EOL, $response);
                $lines = array_slice($lines, 1);
                $countOfRecordsExists = 0;
                foreach ($lines as $line) {

                    $line = explode(',', $line);
                    if (count($line) < 2) {
                        continue;
                    }

                    $dt = \DateTime::createFromFormat('Y-m-d', $line[0]);
                    if (!$dt) {
                        $this->stderr('Can not parse date: ' . $line[0] . PHP_EOL);
                        continue;
                    }

                    $date = $dt->format('Y-m-01');
                    $this->stdout('Processing stock price for: ' . $date . PHP_EOL);

                    // Save new one or update exists stock price
                    $newRecord = false; // the flag will be used to determine the record is new
                    // Try to find stock pirce in the database
                    $stockPriceModel = StockPrice::find()
                        ->where(['date' => $date])
                        ->andWhere(['stock_id' => $stockModel->id])
                        ->one();
                    // If record not found then create new record 
                    if (!$stockPriceModel) {

                        $stockPriceModel = new StockPrice();
                        $stockPriceModel->date = $date;
                        $stockPriceModel->stock_id = $stockModel->id;
                        $stockPriceModel->symbol = $stockModel->symbol;

                        $newRecord = true;
                        $countOfRecordsExists = 0;

                        $this->stdout("The record not found in a database, create new one...\n");

                    } else if (!$fullUpdate) {

                        // Record exists
                        $countOfRecordsExists++;
                        if ($countOfRecordsExists == $countOfExistsRecordToBreakUpdate) {
                            $this->stdout('Max count of exists records is reached, break update process.' . PHP_EOL);
                            break;
                        } else {
                            $this->stdout('Record already exists, count of records exists: ' . $countOfRecordsExists . PHP_EOL);
                        }

                    }

                    // If full update mode enabled or it is a new record
                    if ($fullUpdate || $newRecord) {

                        $stockPriceModel->open = $line[1];
                        $stockPriceModel->high = $line[2];
                        $stockPriceModel->low = $line[3];
                        $stockPriceModel->close = $line[4];
                        $stockPriceModel->volume = $line[5];
                        $stockPriceModel->adj_close = $line[6];

                        if (!$stockPriceModel->validate() || !$stockPriceModel->save()) {
                            $this->stderr('Can not save stock price, errors: ' . json_encode($stockPriceModel->errors) . PHP_EOL);
                            curl_close($curlHandle);
                            return false;
                        } else {
                            $this->stdout('The record saved.' . PHP_EOL, Console::FG_GREEN);
                        }
                    }
                }
            }

            $offset += $limit;

        } while ($offset + $limit < $totalCount);

        curl_close($curlHandle);

        return true;
    }

    /**
     * Downloads csv files from NASDAQ`s site
     *
     * @return boolean
     * @throws ErrorException
     */
    public function actionDownloadStocksData()
    {

        $this->stdout('Downloading stocks data...' . PHP_EOL);

        $curlHandle = curl_init();

        foreach ($this->exchanges as $ex) {

            $this->stdout('Current exchange "' . $ex . '"...' . PHP_EOL);
            $url = 'http://www.nasdaq.com/screening/companies-by-industry.aspx?render=download&exchange=' . $ex;
            try {

                $this->prepareCurl($curlHandle, $url);

                // Download contents from url
                $response = curl_exec($curlHandle);
                if (curl_errno($curlHandle) != CURLE_OK) {
                    throw new ErrorException('Curl error #' . curl_errno($curlHandle) . ', message: ' . curl_error($curlHandle));
                }

                // Get directory path, create if not exists
                $runtimeAliasResolved = Yii::getAlias('@runtime');
                $directoryToStore = $runtimeAliasResolved . '/exchanges';
                if (!file_exists($directoryToStore)) {
                    if (!mkdir($directoryToStore)) {
                        throw new ErrorException('Can not create directory "' . $directoryToStore . '"');
                    }
                }

                // Check directory writable
                if (!is_writable($directoryToStore)) {
                    throw new ErrorException('Can not write to directory "' . $directoryToStore . '"');
                }

                // Save file
                $pathToStore = $directoryToStore . '/' . $ex . '.csv';
                file_put_contents($pathToStore, $response);

            } catch (Exception $e) {
                $this->stderr($e->getMessage() . PHP_EOL);
                curl_close($curlHandle);
                return false;
            }
        }

        curl_close($curlHandle);
        $this->stdout('Work is done!' . PHP_EOL);
        return true;
    }

    /**
     * Loads exchanges data from saved csv files
     *
     * @throws ErrorException
     */
    public function actionLoadExchangesData()
    {
        $this->stdout('Parsing exchanges data...' . PHP_EOL);

        // Get directory path and check directory exists
        $runtimeAliasResolved = Yii::getAlias('@runtime');
        $directoryWithExchangesData = $runtimeAliasResolved . '/exchanges';
        if (!file_exists($directoryWithExchangesData)) {
            $this->stderr('Directory "' . $directoryWithExchangesData . '" not found');
            return false;
        }

        foreach ($this->exchanges as $ex) {

            // Open csv file
            $csvFilePath = $directoryWithExchangesData . '/' . $ex . '.csv';
            if (!file_exists($csvFilePath)) {
                $this->stderr('File "' . $csvFilePath . '" not found' . PHP_EOL);
                continue;
            }
            $this->stdout('Process file: ' . $csvFilePath . PHP_EOL);

            // Read each line
            $fileHandle = fopen($csvFilePath, 'r');
            $isHeader = true;
            while ($line = fgetcsv($fileHandle, null, ',', '"')) {

                if ($isHeader) {
                    $isHeader = false;
                    continue;
                }

                foreach ($line as $key => $value) {
                    $line[$key] = trim($value);
                }

                // Load or create exchange, symbol, sector, industry models
                $exchangeModel = $this->getExchangeModel($ex);
                $this->stdout('Exchange: ' . $exchangeModel->name . PHP_EOL);

                $sector = $line[6];
                $sectorModel = $this->getSectorModel($sector);
                $this->stdout('Sector: ' . $sectorModel->name . PHP_EOL);

                $industry = $line[7];
                $industryModel = $this->getIndustryModel($industry);
                $this->stdout('Industry: ' . $industryModel->name . PHP_EOL);

                // Try to find stock
                $symbol = $line[0];
                $stockModel = Stock::find()
                    ->where(['exchange_id' => $exchangeModel->id])
                    ->andWhere(['symbol' => $symbol])
                    //->andFilterWhere(['sector_id' => $sectorModel->id])
                    //->andFilterWhere(['industry_id' => $industryModel->id])
                    ->one();

                // Stock not found, prepare new one
                if (!$stockModel) {
                    $this->stdout('A new stock will be stored' . PHP_EOL);
                    $stockModel = new Stock();
                    $stockModel->exchange_id = $exchangeModel->id;
                    $stockModel->symbol = $symbol;
                    $stockModel->sector_id = $sectorModel->id;
                    $stockModel->industry_id = $industryModel->id;
                    $stockModel->updated_at = $stockModel->created_at = new DbExpression('NOW()');
                } else {
                    // If stock found, then just say about it
                    $this->stdout('The stock exists, it will be updated' . PHP_EOL);
                }

                $this->stdout('Market Cap: ' . $line[3] . PHP_EOL);
                $this->stdout('Summary Quote: ' . $line[8] . PHP_EOL);
                $this->stdout('Name: ' . $line[1] . PHP_EOL);

                $stockModel->name = $line[1];
                $stockModel->updated_at = new DbExpression('NOW()');
                $stockModel->market_cap = $line[3];
                $stockModel->summary_quote = $line[8];
                if ($line[5] != 'n/a') {
                    $this->stdout('IPO Year: ' . $line[5] . PHP_EOL);
                    $stockModel->ipo_year = $line[5];
                }

                // Save stock
                if (!$stockModel->validate() || !$stockModel->save()) {
                    $this->stderr('Can not save stock, error: ' . json_encode($stockModel->errors) . PHP_EOL);
                    fclose($fileHandle);
                    return false;
                }

            }
            fclose($fileHandle);
        }
        return true;
    }


    /**
     * @param string $name
     * @return array|null|ActiveRecord
     * @throws ErrorException
     */
    protected function getExchangeModel($name)
    {
        return $this->getEntityModel('app\models\Exchange', ['name' => $name]);
    }

    /**
     * @param string $name
     * @return array|null|ActiveRecord
     * @throws ErrorException
     */
    protected function getSectorModel($name)
    {
        return $this->getEntityModel('app\models\Sector', ['name' => $name]);
    }

    /**
     * @param string $name
     * @return array|null|ActiveRecord
     * @throws ErrorException
     */
    protected function getIndustryModel($name)
    {
        return $this->getEntityModel('app\models\Industry', ['name' => $name]);
    }

    /**
     * @param string $class
     * @param array $properties
     * @return ActiveRecord|null|array
     * @throws ErrorException
     */
    protected function getEntityModel($class, array $properties = [])
    {
        $model = $class::find();
        foreach ($properties as $k => $v) {
            $model->andFilterWhere([$k => $v]);
        }
        $model = $model->one();

        if (!$model) {
            $model = new $class();
            foreach ($properties as $k => $v) {
                $model->$k = $v;
            }

            if (!$model->validate() || !$model->save()) {
                throw new ErrorException('Can not save entity of type "' . $class . '", errors: ' . json_encode($model->errors));
            }
        }

        return $model;
    }


    /**
     * @param resource $curlHandle
     * @param string $url
     */
    protected function prepareCurl($curlHandle, $url)
    {
        if (!is_resource($curlHandle)) {
            throw new InvalidParamException('The curl handle is not a resource');
        }

        if (!$url) {
            throw new InvalidParamException('The url is empty');
        }

        curl_setopt_array(
            $curlHandle,
            [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_USERAGENT => Yii::$app->params['user_agent'],
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_CONNECTTIMEOUT => 4,
            ]
        );
    }

    /**
     * Calculates growth direction for each stock for each month from current date
     * to an earlier stock price date
     *
     * @param int $countOfQuartersExistsToBreak
     * @param int $countOfMonthsExistsToBreak
     */
    public function actionCalculateGrowthDirection($countOfQuartersExistsToBreak = 10, $countOfMonthsExistsToBreak = 10)
    {

        $this->stdout('Calculating growth directions for stocks...' . PHP_EOL);

        // Get most early price date
        $result = (new Query())
            ->select('date')
            ->from('stock_price')
            ->orderBy('date asc')
            ->one();
        $earlyDate = $result['date'];
        $earlyDateDt = new \DateTime($earlyDate);
        $this->stdout('The most early price date: ' . $earlyDate . PHP_EOL);
        unset($result, $earlyDate);


        // Get current date
        $dt = new \DateTime('first day of this month');
        $this->stdout('Current date: ' . $dt->format('Y-m-01') . PHP_EOL);

        // The stocks total count will be used to check need to
        $stocksTotal = Stock::find()->count();
        $this->stdout('Need to process ' . $stocksTotal . ' stocks for each month...' . PHP_EOL);

        $monthInterval = \DateInterval::createFromDateString('-1 months');
        
        // Iterate over each month from $earlyDateDt to $dt
        do {

            $this->stdout('Processing stocks for the month: ' . $dt->format('Y-m') . PHP_EOL);

             // The values will be used to select stocks list from database
            $limit = 100;
            $offset = 0;

            $dtAsString = $dt->format('Y-m-01');

            // Iterate over each stock
            do {

                $stocks = Stock::find()
                    ->offset($offset)
                    ->limit($limit)
                    ->all();

                foreach ($stocks as $stock) {
                    /** @var Stock $stock */
                    $this->stdout('Selecting prices for stock "' . $stock->symbol . '"...' . PHP_EOL);
                    // Check prices for the current month are exists
                    $exists = (new Query())
                        ->from('stock_price')
                        ->where(['date' => $dtAsString])
                        ->andWhere(['stock_id' => $stock->id])
                        ->exists();
                    if (!$exists) {
                        $this->stderr('No prices for "' . $dtAsString . '", can not continue' . PHP_EOL);
                        continue;
                    }       
                    // Select prices for the last 3 years (24 months)
                    $threeYearsAgoDt = clone $dt;
                    $threeYearsAgoDt->add(\DateInterval::createFromDateString('-3 years'));
                    $query = (new Query())
                        ->select(['date', 'open'])
                        ->from('stock_price')
                        ->where(['<=', 'date', $dtAsString])
                        ->andWhere(['>=', 'date', $threeYearsAgoDt->format('Y-m-01')])
                        ->andWhere(['stock_id' => $stock->id])
                        ->orderBy('date asc');
                    if (!$query->count()) {
                        $this->stderr('No prices found' . PHP_EOL);
                        continue;
                    }     
                    // Create set of prices
                    $result = $query->all();
                    $prices = [];
                    foreach ($result as $row) {
                        $prices[] = $row['open'];
                    }
                    unset($result, $yearAgoDt);

                    $this->stdout('Prices set for the 3 years: ' . implode(', ', $prices) . PHP_EOL);

                    // A function to calculate growth direction
                    $getGrowthDirection = function (array $set) {

                        $growthDirection = 0;
                        if (StockGrowthDirection::isSetGrowUp($set)) {
                            $growthDirection = StockGrowthDirection::GROWTH_DIRECTION_UP;
                            $this->stdout('Direction is "UP".' . PHP_EOL);
                        } else if (StockGrowthDirection::isSetGrowDown($set)) {
                            $growthDirection = StockGrowthDirection::GROWTH_DIRECTION_DOWN;
                            $this->stdout('Direction is "DOWN".' . PHP_EOL);
                        }
                        // If can not determine growth direction...
                        if (!$growthDirection) {
                            $this->stderr('Can not determine growth direction, no reason to continue process for the stock' . PHP_EOL);
                        }

                        return $growthDirection;
                    };

                    // A function to save growth direction
                    $saveGrowthDirectionModel = function (Stock $stock, $dtAsString, $growthDirection, $countPropertyName, $count) {

                        $stockGrowthDirectionModel = new StockGrowthDirection();
                        $stockGrowthDirectionModel->stock_id = $stock->id;
                        $stockGrowthDirectionModel->growth_direction = $growthDirection;
                        $stockGrowthDirectionModel->from = $dtAsString;
                        $stockGrowthDirectionModel->$countPropertyName = $count;
                        if (!$stockGrowthDirectionModel->validate()) {
                            $this->stderr(
                                'Can not validate data for growth direction model, errors: ' 
                                    . json_encode($stockGrowthDirectionModel->errors) 
                                    . PHP_EOL
                            );
                            return false;
                        }
                        if (!$stockGrowthDirectionModel->save()) {
                            $this->stderr(
                                'Can not save growth direction model, errors: ' 
                                    . json_encode($stockGrowthDirectionModel->errors)
                                    . PHP_EOL
                            );
                            return false;
                        }
                        // Growth direction model saved
                        $this->stdout('The growth direction model saved' . PHP_EOL);
                    };

                    // Calculate growth direction for each year quarter
                    $this->stdout('Calculating growth directions for year quarters...' . PHP_EOL);
                    // Get quarter prices list
                    $pricesCount = count($prices);
                    $quartersCount = floor($pricesCount / 3);
                    $this->stdout('Quarters count: ' . $quartersCount . PHP_EOL);
                    $quarterPrices = [];
                    $left = 3;
                    for ($i = $pricesCount - 1; $i >= 0; --$i, $left++) {
                        if ($left == 3) {
                            $quarterPrices[] = $prices[$i];
                            $left = 0;
                        }
                    }
                    $quarterPrices = array_reverse($quarterPrices);
                    $this->stdout('Quarter prices: ' . implode(', ', $quarterPrices) . PHP_EOL);
                    
                    // Save growth directions
                    $countOfQuartersExists = 0;
                    for ($i = 1; $i <= $quartersCount; ++$i) {
                        
                        // Check data already exists
                        $exists = StockGrowthDirection::find()
                            ->where(['from' => $dtAsString])
                            ->andWhere(['count_of_quarters' => $i + 1])
                            ->andWhere(['stock_id' => $stock->id])
                            ->exists();
                        if ($exists) {
                            $this->stdout(
                                'A data for the stock "' . $stock->symbol . '" from "' . $dtAsString . '" '
                                    . ' for ' . $i . ' quarter(s) already exists' . PHP_EOL
                            );

                            $countOfQuartersExists++;
                            if ($countOfQuartersExists == $countOfQuartersExistsToBreak) {
                                $this->stdout('Max count of quarters exists is reached.' . PHP_EOL);
                                break;
                            }

                            continue;
                        } else {
                            $countOfQuartersExists = 0;
                        }

                        $iPlusOne = $i + 1;
                        $pricesSetForTheCurrentQuartersCount = array_slice($quarterPrices, -$iPlusOne, $iPlusOne);
                        $this->stdout(
                            'Prices set for the current quarters count (' . $i . '): ' . implode(', ', $pricesSetForTheCurrentQuartersCount)
                                . PHP_EOL
                        );
                        // Determine set`s growth direction
                        $growthDirection = $getGrowthDirection($pricesSetForTheCurrentQuartersCount);
                        if (!$growthDirection) {
                            break;
                        }
                        // Has growth direction, save data
                        $saveGrowthDirectionModel($stock, $dtAsString, $growthDirection, 'count_of_quarters', $i);
                    }
                    unset($quartersCount, $quarterPrices);

                    // Slice set for each months count and calculate growth direction for each months count
                    if ($pricesCount > 13) {
                        // We need data only for the first year
                        $pricesCount = 13;
                    }
                    $monthsCount = $pricesCount - 1;
                    $countOfMonthsExists = 0;
                    for ($i = 1; $i < $monthsCount; ++$i) {
                        // Check data already exists
                        $exists = StockGrowthDirection::find()
                            ->where(['from' => $dtAsString])
                            ->andWhere(['count_of_months' => $i])
                            ->andWhere(['stock_id' => $stock->id])
                            ->exists();
                        if ($exists) {
                            $this->stdout(
                                'A data for the stock "' . $stock->symbol . '" from "' . $dtAsString
                                    . '" for ' . $i . ' month(s) already exists' . PHP_EOL
                            );

                            $countOfMonthsExists++;
                            if ($countOfMonthsExists == $countOfMonthsExistsToBreak) {
                                $this->stdout('Max count of months exists is reached.' . PHP_EOL);
                                break;
                            }

                            continue;

                        } else {
                            $countOfMonthsExists = 0;
                        } 


                        // Slice prices list for the current months count
                        $iPlusOne = $i + 1;
                        $pricesSetForTheCurrentMonthsCount = array_slice($prices, -$iPlusOne, $iPlusOne);
                        $this->stdout(
                            'Prices set for last ' . $i . ' month(s): ' 
                                . implode(', ', $pricesSetForTheCurrentMonthsCount)
                                . PHP_EOL
                        );
                        // Determine set`s growth direction
                        $growthDirection = $getGrowthDirection($pricesSetForTheCurrentMonthsCount);
                        if (!$growthDirection) {
                            break;
                        }
                        // Has growth direction, save data
                        $saveGrowthDirectionModel($stock, $dtAsString, $growthDirection, 'count_of_months', $i);
                    }
                }    

                $offset += $limit;

            } while ($offset + $limit < $stocksTotal);

            $dt->add($monthInterval);

        } while($dt > $earlyDateDt);

        return true;
    }

}
