<?php
/** @var $stock app\models\Stock */
/** @var $roi app\models\StockRoi */
/** @var $index int **/

use app\models\StockChart;
?>
<?php if (isset($stock->charts[StockChart::$types['TYPE_YEARLY_3']])): ?>
<a class="anchor-chart-modal-opener"
	id="chart-modal-opener-<?= $index ?>" 
	data-index="<?= $index ?>"
	data-symbol="<?= $stock->symbol ?>" 
	data-roi-one-month="<?= round($roi->roi_1m, 2) ?>"
	data-roi-three-months="<?= round($roi->roi_3m, 2) ?>"
	data-roi-six-months="<?= round($roi->roi_6m, 2) ?>"
	data-roi-one="<?= round($roi->roi_1, 2) ?>" 
	data-roi-two="<?= round($roi->roi_2, 2) ?>"
	data-roi-three="<?= round($roi->roi_3, 2) ?>"  
	data-roi-best="<?= round($roi->roi_best, 2) ?>"
	data-roi-worst="<?= round($roi->roi_worst, 2) ?>"
	href="<?= $stock->charts[StockChart::$types['TYPE_YEARLY_3']]->getChartFileUrl() ?>">
    <img src="<?= $stock->charts[StockChart::$types['TYPE_YEARLY_3']]->getChartThumbnailUrl(200, 100) ?>" alt="" />
</a>    
<?php else: ?>
(<?= Yii::t('app', 'not set') ?>)
<?php endif; ?>