<?php
/** @var $stockHistory \app\models\StockHistory */

use yii\helpers\Url;
use app\models\StockChart;

$this->registerJsFile('@web/js/chart-anchors-stock.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->params['breadcrumbs'] = [
	[
		'label' => Yii::t('app', $stockHistory->stock->symbol)
	]
];
$this->title = 'Stock Informer: ' . Yii::t('app', '{symbol} details', ['symbol' => $stockHistory->stock->symbol]);
?>
<div class="site-index">
	<div class="body-content">
		<h1><?= $stockHistory->stock->symbol ?></h1>
		<div class="row">
			<div class="col-xs-3">
				<?php 
				echo \yii\grid\GridView::widget(
					[
						'dataProvider' => $stockHistory->dataProvider, 
						'columns' => [
							'date', 
							[
								'class' => \yii\grid\DataColumn::className(),
								'attribute' => 'price',
								'value' => function ($model) {
									return Yii::$app->formatter->asDecimal($model['price'], 2);
								},
							], 
							[
								'class' => \yii\grid\DataColumn::className(),
								'attribute' => 'roi_1m',
								'content' => function ($model) {
									$html = '<span class="value-' . ($model['roi_1m'] >= 100 ? 'green' : 'red') . '">';
									$html .= Yii::$app->formatter->asDecimal($model['roi_1m'], 2) . '%';
									return $html .= '</span>';
								},
							],
						],
						'pager' => [
							'maxButtonCount' => 6
						],
					]
				); 
				?>
			</div>
			<div class="col-xs-9">
				<h2><?= Yii::t('app', 'Information') ?></h2>
				<div class="list-group">

					<li class="list-group-item">
						<b><?= Yii::t('app', 'AVG ROI') ?>/<?= Yii::t('app', 'Month') ?>:</b> 
						<?= Yii::$app->formatter->asDecimal($stockHistory->stock->averageRoi, 2) ?>%
					</li>
					<?php
					$valuesToShow = [
						'eps' => 'EPS',
						'roi_best' => 'BestROI',
						'roi_worst' => 'WorstROI',
						'roi_1m' => 'ROI1M',
						'roi_3m' => 'ROI3M',
						'roi_6m' => 'ROI6M',
						'roi_1' => 'ROI1',
					];
					?>
					<?php foreach ($valuesToShow as $property => $label): ?>
						<?php if ($stockHistory->stock->lastRoi->{$property}): ?>
							<div class="list-group-item">
								<b><?= Yii::t('app', $label) ?>:</b>
								<?= Yii::$app->formatter->asDecimal($stockHistory->stock->lastRoi->{$property}, 2) ?><?php if (strpos($property, 'roi') !== false): ?>%<?php endif; ?>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>

					<a class="list-group-item list-group-item-info" 
						href="http://elite.finviz.com/quote.ashx?t=<?= $stockHistory->stock->symbol ?>&ty=p&ta=0&p=d" 
						target="_blank"><?= Yii::t('app', 'Performance') ?></a>

					<a class="list-group-item list-group-item-info"
						href="http://www.google.com/finance?q=<?= $stockHistory->stock->symbol ?>"
						target="_blank"><?= Yii::t('app', 'Google Finance') ?></a>

					<?php
						$currentDt = new \DateTime();
						$threeYearsAgoDt = clone $currentDt;
						$threeYearsAgoDt->sub(DateInterval::createFromDateString('+3 years'));
					?>
					<a class="list-group-item list-group-item-info"
						href='http://finance.yahoo.com/echarts?s=<?= $stockHistory->stock->symbol ?>+Interactive#{"customRangeStart":<?= $threeYearsAgoDt->format('U') ?>,"customRangeEnd":<?= $currentDt->format('U') ?>,"range":"custom","allowChartStacking":true}'
						target="_blank">Yahoo</a>

				</div>

				<h2><?= Yii::t('app', 'Charts') ?></h2>
				<?php 
					$chartTypes = [
						'TYPE_DAILY', 'TYPE_WEEKLY'
					];
				?>
				<?php foreach ($chartTypes as $type): ?>
					<?php if (isset($stockHistory->stock->charts[StockChart::$types[$type]])): ?>
						<?php 
							$label = str_replace('TYPE_', null, $type);
							$label = ucfirst(strtolower($label));
						?>
						<?php $chart = $stockHistory->stock->charts[StockChart::$types[$type]]; ?>
						<div class="thumbnail">
							<a class="chart-modal-opener" data-symbol="<?= $stockHistory->stock->symbol ?>" data-type="<?= $label ?>" href="<?= $chart->getChartFileUrl() ?>" target="_blank">
								<img src="<?= $chart->getChartThumbnailUrl(800, 700) ?>" alt="<?= Yii::t('app', $label) ?>">
							</a>
							<div class="caption">
								<h3><?= Yii::t('app', $label) ?></h3>
							</div>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>	
		</div>			
	</div>
</div>
<?php 
	\yii\bootstrap\Modal::begin(
		[
			'id' => 'modal-chart',
			'size' => \yii\bootstrap\Modal::SIZE_LARGE,
			'header' => '<h4></h4>',
			'options' => [
				'class' => 'modal-centered'
			],
		]
	); 
?>
<?php \yii\bootstrap\Modal::end(); ?>