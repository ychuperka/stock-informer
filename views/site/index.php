<?php

/** @var $this yii\web\View */
/** @var $searchForm app\models\SearchForm */
/** @var $sortFieldName string */
/** @var $sortPeriod array */
/** @var $sortDirection string */
/** @var $stockModels array */
/** @var $industryModels array */
/** @var $roiPeriods array */
/** @var $dateTo \DateTime|null */
/** @var $dateToMinusThreeYears \DateTime|null */
/** @var $showCharts boolean */
/** @var $title string */

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\SearchForm;
use app\models\StockGrowthDirection;

$this->title = 'Stock Informer' . ($title ? (': ' . $title) : null);
$this->registerJsFile('@web/js/chart-anchors.js', ['depends' => \yii\web\JqueryAsset::className()]);
$prepareROIValue = function ($model, $key, $index, $column) {
    $value = $model->{$column->attribute};
    if ($value) {
        return Yii::$app->formatter->asDecimal($value, 2) . '%';
    } else {
        return null;
    } 
};

$columns = [
    [
        'class' => \yii\grid\SerialColumn::className(),
    ],
    [
        'class' => \yii\grid\Column::className(),
        'header' => Yii::t('app', 'Stock'),
        'content' => function ($model, $key, $index, $column) use ($dateTo, $dateToMinusThreeYears, $stockModels, $industryModels) {
            
            $stock = $stockModels[$model->id];

            $urlToGoogle = 'http://www.google.com/finance?q=' . $stock->symbol;
            $urlToMsn = 'http://www.msn.com/en-us/money/stockdetails/?symbol=' . $stock->symbol;
            $urlToFinViz = 'http://finviz.com/quote.ashx?t=' . $stock->symbol;    
            $urlToStockPage = Url::toRoute(['site/stock', 'stock_id' => $stock->id]);  
            $urlToCsv = 'http://ichart.yahoo.com/table.csv?g=m&s=' . $stock->symbol;  
           
            $symbolAnchor = Html::a($stock->symbol, $urlToGoogle, ['target' => '_blank']);
            $stockPageAnchor = Html::a(Yii::t('app', 'Info'), $urlToStockPage, ['target' => '_blank']);
            $msnAnchor = Html::a('MSN', $urlToMsn, ['target' => '_blank']);
            $finVizAnchor = Html::a('FinViz', $urlToFinViz, ['target' => '_blank']);
            $csvAnchor = Html::a('CSV', $urlToCsv, ['target' => '_blank']);

            if ($dateTo) {
                $urlToYahoo = 'http://finance.yahoo.com/echarts?s=' 
                    . $stock->symbol . '+Interactive#{"customRangeStart":'
                    . $dateToMinusThreeYears->format('U') . ',"customRangeEnd":' 
                    . $dateTo->format('U') . ',"range":"custom","allowChartStacking":true}';
                $yahooAnchorHtml = Html::tag('div', Html::a('Yahoo', $urlToYahoo, ['target' => '_blank']), ['class' => 'anchor-container-yahoo']);    
            } else {
                $yahooAnchorHtml = null;
            }
           
            $industry = $industryModels[$stock->id];
            $content = 
                Html::tag('div', $symbolAnchor, ['class' => 'anchor-container-symbol']) . '&nbsp;'
                . $yahooAnchorHtml
                . $stockPageAnchor . '&nbsp;'
                . $msnAnchor . '&nbsp;'
                . $finVizAnchor . '&nbsp;'
                . $csvAnchor . '&nbsp;'
                . PHP_EOL 
                . $stock->name . PHP_EOL 
                . $industry->name . PHP_EOL
                . '<b>' . Yii::t('app', 'Price') . ':</b> ' . ($stock->stock_price ? Yii::$app->formatter->asDecimal($stock->stock_price, 2) : Yii::t('app', 'Unknown')) . PHP_EOL
                . '<b>' .Yii::t('app', 'Capitalisation') . ':</b> ' . ($stock->market_cap ? Yii::$app->formatter->asDecimal($stock->market_cap, 2) : Yii::t('app', 'Unknown')) . PHP_EOL
                . '<b>' .Yii::t('app', 'Volume') . ':</b> ' . ($stock->volume ? Yii::$app->formatter->asDecimal($stock->volume, 2) : Yii::t('app', 'Unknown')) . PHP_EOL
                . '<b>' .Yii::t('app', 'BestROI') . ':</b> ' . ($model->roi_best ? Yii::$app->formatter->asDecimal($model->roi_best, 2) . '%' : Yii::t('app', 'Unknown')) . PHP_EOL
                . '<b>' .Yii::t('app', 'WorstROI') . ':</b> ' . ($model->roi_worst ? Yii::$app->formatter->asDecimal($model->roi_worst, 2) . '%' : Yii::t('app', 'Unknown'));
            return nl2br($content);    
        },  
    ],
    [
        'class' => \yii\grid\DataColumn::className(),
        'attribute' => 'roi_3',
        'label' => 'ROI3',
        'value' => $prepareROIValue,
    ],
    [
        'class' => \yii\grid\DataColumn::className(),
        'attribute' => 'roi_2',
        'label' => 'ROI2',
        'value' => $prepareROIValue,
    ],
    [
        'class' => \yii\grid\DataColumn::className(),
        'attribute' => 'roi_1',
        'label' => 'ROI1',
        'value' => $prepareROIValue,
    ],
    [
        'class' => \yii\grid\DataColumn::className(),
        'attribute' => 'roi_1m',
        'label' => 'ROI1M',
        'value' => $prepareROIValue,
    ],
    [
        'class' => \yii\grid\DataColumn::className(),
        'attribute' => 'roi_3m',
        'label' => 'ROI3M',
        'value' => $prepareROIValue,
    ],
    [
        'class' => \yii\grid\DataColumn::className(),
        'attribute' => 'roi_6m',
        'label' => 'ROI6M',
        'value' => $prepareROIValue,
    ],
    [
        'class' => \yii\grid\DataColumn::className(),
        'attribute' => 'roi_3years',
        'label' => 'ROI3Y',
        'value' => $prepareROIValue,
    ],
    [
        'class' => \yii\grid\DataColumn::className(),
        'attribute' => 'roi_best',
        'label' => Yii::t('app', 'BestROI'),
        'value' => $prepareROIValue,
    ],
    [
        'class' => \yii\grid\DataColumn::className(),
        'attribute' => 'roi_worst',
        'label' => Yii::t('app', 'WorstROI'),
        'value' => $prepareROIValue,
    ],
    [
        'class' => \yii\grid\DataColumn::className(),
        'attribute' => 'eps',
        'label' => 'EPS',
        'value' => function ($model, $key, $index, $column) use ($stockModels) {
            return $model->eps !== null ? round($model->eps, 2) : null;
        },
    ],
];
if ($showCharts) {
    $columns[] = [
        'class' => \yii\grid\Column::className(),
        'header' => Yii::t('app', 'Chart'),
        'content' => function ($model, $key, $index, $column) use ($stockModels) {
            return $this->render(
                'chart_cell', 
                [
                    'stock' => $stockModels[$model->id], 
                    'roi' => $model, 
                    'index' => $index,
                ]
            );
        },
        'contentOptions' => [
            'class' => 'centered'
        ],
    ];
}
$gridWidget = \yii\grid\GridView::widget(
    [
        'dataProvider' => $searchForm->dataProvider,
        'columns' => $columns,
    ]
);
?>
<div class="site-index">

    <div class="body-content">
        <?php
        $prepareRoiPeriod = function (array $period, $template = 'y') {
            return "({$period[0]->format($template)} &mdash; {$period[1]->format($template)})";
        };
        $form = ActiveForm::begin(
            [
                'id' => 'search-form',
                'method' => 'get',
                'action' => Url::toRoute('site/index'),
            ]
        );
        ?>
            <div class="row">
                <div class="col-lg-1">
                    <?php
                        echo $form->field(
                            $searchForm, 'symbol',
                            [
                                'inputOptions' => [
                                    'class' => 'form-control', 
                                    'title' => Yii::t('app', 'Symbol'),
                                ]
                            ]
                        )
                        ->label(Yii::t('app', 'Symbol'));
                    ?>
                </div>    
                <div class="col-lg-1">
                    <?php
                        echo $form->field(
                            $searchForm, 'stock_price',
                            [
                                'inputOptions' => [
                                    'class' => 'form-control', 
                                    'title' => Yii::t('app', 'Min Stock Price'),
                                ]
                            ]
                        )
                        ->label(Yii::t('app', 'Min Price'));
                    ?>
                </div> 
                <div class="col-lg-1">
                    <?php
                        echo $form->field(
                            $searchForm, 'market_cap',
                            [
                                'inputOptions' => [
                                    'class' => 'form-control', 
                                    'title' => Yii::t('app', 'Min Capitalisation'),
                                ]
                            ]
                        )
                        ->label(Yii::t('app', 'Min Cap'));
                    ?>
                </div> 
                <div class="col-lg-1">
                    <?php
                        echo $form->field(
                            $searchForm, 'volume',
                            [
                                'inputOptions' => [
                                    'class' => 'form-control', 
                                    'title' => Yii::t('app', 'Min Volume'),
                                ]
                            ]
                        )
                        ->label(Yii::t('app', 'Min Vol'));
                    ?>
                </div> 
                <div class="col-lg-1">
                    <?php
                        echo $form->field($searchForm, 'roi_max')
                            ->widget(\kartik\switchinput\SwitchInput::className());
                    ?>
                </div>
                <div class="col-lg-2 col-lg-offset-1">
                    <?php
                        echo $form->field($searchForm, 'date_to')
                            ->widget(
                                \trntv\yii\datetime\DateTimeWidget::className(),
                                [
                                    'momentDatetimeFormat' => 'YYYY-MM-01',
                                    'phpDatetimeFormat' => 'php:Y-m-d',
                                ]
                            );
                    ?>
                </div> 
            </div>
            <div class="row">
                <div class="col-lg-1">
                    <?php 
                        echo $form->field(
                            $searchForm, 'roi_1m', 
                            [
                                'inputOptions' => [
                                    'class' => 'form-control', 
                                    'title' => Yii::t('app', 'ROI1M'),
                                ]
                            ]
                        )
                        ->hint($prepareRoiPeriod($roiPeriods['roi_1m'], 'm'))
                        ->label(Yii::t('app', 'ROI1M')); 
                    ?>
                </div>    
                <div class="col-lg-1">
                    <?php 
                        echo $form->field(
                            $searchForm, 'roi_3m', 
                            [
                                'inputOptions' => [
                                    'class' => 'form-control', 
                                    'title' => Yii::t('app', 'ROI3M'),
                                ]
                            ]
                        )
                        ->hint($prepareRoiPeriod($roiPeriods['roi_3m'], 'm'))
                        ->label(Yii::t('app', 'ROI3M')); 
                    ?>
                </div>
                <div class="col-lg-1">
                    <?php 
                        echo $form->field(
                            $searchForm, 'roi_6m', 
                            [
                                'inputOptions' => [
                                    'class' => 'form-control', 
                                    'title' => Yii::t('app', 'ROI6M'),
                                ]
                            ]
                        )
                        ->hint($prepareRoiPeriod($roiPeriods['roi_6m'], 'm'))
                        ->label(Yii::t('app', 'ROI6M')); 
                    ?>
                </div>  
                <div class="col-lg-1">
                    <?php 
                        echo $form->field(
                            $searchForm, 'roi_1', 
                            [
                                'inputOptions' => [
                                    'class' => 'form-control', 
                                    'title' => Yii::t('app', 'ROI1'),
                                ]
                            ]
                        )
                        ->hint($prepareRoiPeriod($roiPeriods['roi_1']))
                        ->label(Yii::t('app', 'ROI1')); 
                    ?>
                </div>
                <div class="col-lg-1">
                    <?php 
                        echo $form->field(
                            $searchForm, 'roi_2', 
                            [
                                'inputOptions' => [
                                    'class' => 'form-control', 
                                    'title' => Yii::t('app', 'ROI2'),
                                ]
                            ]
                        )
                        ->hint($prepareRoiPeriod($roiPeriods['roi_2']))
                        ->label(Yii::t('app', 'ROI2')); 
                    ?>
                </div>
                <div class="col-lg-1">
                    <?php 
                        echo $form->field(
                            $searchForm, 'roi_3', 
                            [
                                'inputOptions' => [
                                    'class' => 'form-control', 
                                    'title' => Yii::t('app', 'ROI3'),
                                ]
                            ]
                        )
                        ->hint($prepareRoiPeriod($roiPeriods['roi_3']))
                        ->label(Yii::t('app', 'ROI3')); 
                    ?>
                </div>
                <div class="col-lg-2">
                    <?php 
                        echo $form->field(
                            $searchForm, 'roi_3years', 
                            [
                                'inputOptions' => [
                                    'class' => 'form-control', 
                                    'title' => Yii::t('app', 'ROI3Y'),
                                ]
                            ]
                        )
                        ->hint($prepareRoiPeriod($roiPeriods['roi_3years'], 'y.m'))
                        ->label(Yii::t('app', 'ROI3Y')); 
                    ?>
                </div> 
                <div class="col-lg-1">
                    <?php
                        echo $form->field(
                            $searchForm, 'roi_best_min',
                            [
                                'inputOptions' => [
                                    'title' => Yii::t('app', 'Best ROI Min.'),
                                    'placeholder' => Yii::t('app', 'Min'),
                                ],
                            ]
                        )->label(Yii::t('app', 'BestROI'));
                    ?>
                </div> 
                <div class="col-lg-1 input-without-label-container">
                    <?php
                        echo $form->field(
                            $searchForm, 'roi_best_max',
                            [
                                'inputOptions' => [
                                    'title' => Yii::t('app', 'Best ROI Max.'),
                                    'placeholder' => Yii::t('app', 'Max'),
                                ],
                            ]
                        )->label(Yii::t('app', 'BestROI'), ['class' => 'sr-only']);
                    ?>
                </div> 
                <div class="col-lg-1">
                    <?php
                        echo $form->field(
                            $searchForm, 'roi_worst_min',
                            [
                                'inputOptions' => [
                                    'title' => Yii::t('app', 'Worst ROI Min.'),
                                    'placeholder' => Yii::t('app', 'Min'),
                                ],
                            ]
                        )->label(Yii::t('app', 'WorstROI'));
                    ?>
                </div> 
                <div class="col-lg-1 input-without-label-container">
                    <?php
                        echo $form->field(
                            $searchForm, 'roi_worst_max',
                            [
                                'inputOptions' => [
                                    'title' => Yii::t('app', 'Worst ROI Max.'),
                                    'placeholder' => Yii::t('app', 'Max'),
                                ],
                            ]
                        )->label(Yii::t('app', 'WorstROI'), ['class' => 'sr-only']);
                    ?>
                </div> 
            </div>  
            <div class="row">
                <div class="col-lg-2">
                    <?php
                        echo $form->field($searchForm, 'go_direction')
                            ->dropDownList(
                                [
                                    StockGrowthDirection::GROWTH_DIRECTION_UP => 'Up', 
                                    StockGrowthDirection::GROWTH_DIRECTION_DOWN => 'Down',
                                ],
                                ['prompt' => 'Select...']
                        );
                    ?>
                </div>
                <div class="col-lg-2">
                    <?php
                        $months = [];
                        for ($i = 1; $i < 13; ++$i) {
                            $months["$i"] = $i;
                        }
                        echo $form->field($searchForm, 'count_of_months_to_go')
                            ->dropDownList(
                                $months,
                                [
                                    'prompt' => 'Month(s)...'
                                ]
                            )->label(Yii::t('app', 'for last N Month(s)'));
                    ?>
                </div> 
                <div class="col-lg-2">
                    <?php
                        $quarters = [];
                        for ($i = 1; $i < 13; ++$i) {
                            $quarters["$i"] = $i;
                        }
                        echo $form->field($searchForm, 'count_of_quarters_to_go')
                            ->dropDownList(
                                $quarters,
                                [
                                    'prompt' => 'Quarter(s)...'
                                ]
                        )->label(Yii::t('app', 'for last N Quarter(s)'));
                    ?>
                </div>    
                <div class="col-lg-2">
                    <?php
                        echo $form->field($searchForm, 'eps_direction')
                            ->dropDownList(
                                [
                                    SearchForm::EPS_DIRECTION_LESSER => '<',
                                    SearchForm::EPS_DIRECTION_GREATER => '>',
                                ],
                                ['prompt' => 'Select...']
                        );
                    ?> 
                </div>    
                <div class="col-lg-1">
                    <?php
                        echo $form->field($searchForm, 'eps');
                    ?>
                </div>    
                <div class="col-lg-1 col-lg-offset-1">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                </div>   
            </div>    
        <?php ActiveForm::end() ?>
        <?php if ($sortFieldName): ?>
        <div class="row">
            <div class="col-xs-12">
                <?= Yii::t('app', 'Sort by') ?> <b><?= $sortFieldName ?> <?= strtoupper($sortDirection) ?></b> (<?= $sortPeriod[0]->format('Y-m-d') ?> &mdash; <?= $sortPeriod[1]->format('Y-m-d') ?>)
            </div>    
        </div>  
        <?php endif; ?>
        <div class="row">
            <div class="col-xs-12">
                <?= $gridWidget ?>
            </div>    
        </div>    
    </div>
</div>
<?php 
\yii\bootstrap\Modal::begin(
    [
        'id' => 'modal-chart',
        'size' => \yii\bootstrap\Modal::SIZE_LARGE,
        'header' => '<h4><a class="left" href="#">&larr;</a> <a class="right" href="#">&rarr;</a> ' . Yii::t('app', 'A chart for stock') . ' <span></span><h4>',
        'footer' => '<p></p>',
        'options' => [
            'class' => 'modal-centered'
        ],
    ]
);
\yii\bootstrap\Modal::end();
?>
