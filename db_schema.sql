DROP TABLE IF EXISTS `sector`;
CREATE TABLE `sector` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY (`name`)
) ENGINE = InnoDB CHARACTER SET `utf8`;

DROP TABLE IF EXISTS `industry`;
CREATE TABLE `industry` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY (`name`)
) ENGINE = InnoDB CHARACTER SET `utf8`;

DROP TABLE IF EXISTS `exchange`;
CREATE TABLE `exchange` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY (`name`)
) ENGINE = InnoDB CHARACTER SET `utf8`;

DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sector_id` INT(11) UNSIGNED NOT NULL,
  `industry_id` INT(11) UNSIGNED NOT NULL,
  `exchange_id` INT(11) UNSIGNED NOT NULL,
  `market_cap` DECIMAL(24, 4) UNSIGNED NOT NULL,
  `symbol` VARCHAR(32) NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `ipo_year` SMALLINT(4) UNSIGNED DEFAULT NULL,
  `summary_quote` VARCHAR(255) DEFAULT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `stock_price` DECIMAL(24, 4) DEFAULT NULL,
  `volume` DECIMAL(24, 4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`sector_id`) REFERENCES `sector` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`industry_id`) REFERENCES `industry` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`exchange_id`) REFERENCES `exchange` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  KEY (`exchange_id`, `sector_id`, `industry_id`),
  UNIQUE KEY (`exchange_id`, `symbol`),
  KEY (`symbol`),
  KEY (`ipo_year`),
  KEY (`created_at`),
  KEY (`updated_at`),
  KEY (`stock_price`),
  KEY (`volume`)
) ENGINE = InnoDB CHARACTER SET `utf8`;

DROP TABLE IF EXISTS `stock_roi`;
CREATE TABLE `stock_roi` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stock_id` INT(11) UNSIGNED NOT NULL,
  `date` DATE NOT NULL,
  `roi_1` DECIMAL(24, 4) DEFAULT NULL,
  `roi_2` DECIMAL(24, 4) DEFAULT NULL,
  `roi_3` DECIMAL(24, 4) DEFAULT NULL,
  `roi_1m` DECIMAL(24, 4) DEFAULT NULL,
  `roi_3m` DECIMAL(24, 4) DEFAULT NULL,
  `roi_6m` DECIMAL(24, 4) DEFAULT NULL,
  `roi_3years` DECIMAL(24, 4) DEFAULT NULL,
  `roi_best` DECIMAL(24, 4) DEFAULT NULL,
  `roi_worst` DECIMAL(24, 4) DEFAULT NULL,
  `eps` DECIMAL(10, 4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`)
    ON UPDATE CASCADE ON DELETE CASCADE,
  KEY (`date`),
  UNIQUE KEY (`stock_id`, `date`),  
  KEY (`roi_1`),
  KEY (`roi_2`),
  KEY (`roi_3`),
  KEY (`roi_1m`),
  KEY (`roi_3m`),
  KEY (`roi_6m`),
  KEY (`roi_3years`),
  KEY (`roi_best`),
  KEY (`roi_worst`),
  KEY (`eps`)
) ENGINE = InnoDB CHARACTER SET `utf8`;

DROP TABLE IF EXISTS `stock_growth_direction`;
CREATE TABLE `stock_growth_direction` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `stock_id` INT(11) UNSIGNED NOT NULL,
    `created_at` DATETIME NOT NULL,
    `growth_direction` TINYINT(1) UNSIGNED NOT NULL, # 1 - Up, 2 - Down
    `from` DATE NOT NULL,
    `count_of_months` TINYINT(2) UNSIGNED DEFAULT NULL,
    `count_of_quarters` TINYINT(1) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`)
      ON DELETE CASCADE ON UPDATE CASCADE,
    KEY (`stock_id`, `from`, `count_of_months`),
    KEY (`stock_id`, `from`, `count_of_quarters`)
) ENGINE = InnoDB CHARACTER SET `utf8`;

DROP TABLE IF EXISTS `stock_price`;
CREATE TABLE `stock_price` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stock_id` INT(11) UNSIGNED NOT NULL,
  `date` DATE NOT NULL,
  `open` DECIMAL(24, 4) NOT NULL,
  `high` DECIMAL(24, 4) NOT NULL,
  `low` DECIMAL(24, 4) NOT NULL,
  `close` DECIMAL(24, 4) NOT NULL,
  `volume` DECIMAL(24, 4) NOT NULL,
  `adj_close` DECIMAL(24, 4) NOT NULL,
  `symbol` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`stock_id`) REFERENCES `stock` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  KEY (`date`),
  KEY (`stock_id`),
  KEY (`date`),
  UNIQUE KEY (`stock_id`, `date`),
  KEY (`symbol`),
  KEY (`open`),
  KEY (`high`),
  KEY (`low`),
  KEY (`close`),
  KEY (`volume`),
  KEY (`adj_close`)
) ENGINE = InnoDB CHARACTER SET `utf8`;
