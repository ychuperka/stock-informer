(function ($) {
	$(function () {

		var modal = $('div#modal-chart');

		$('a.chart-modal-opener').click(function (e) {
			e.preventDefault();

			var self = $(this);

			modal.find('div.modal-header h4').text(self.data('symbol') + ' ' + self.data('type'));
			modal.find('div.modal-body').html('<img src="' + self.attr('href') + '" alt="' + self.data('type') + '">');

			modal.modal();

		});

	});
})(jQuery);