(function ($) {
    $(function () {

        var maxIndex = 0;
        $('a.anchor-chart-modal-opener').each(function (index) {
            var index = parseInt($(this).data('index'));
            if (index > maxIndex) {
                maxIndex = index;
            }
        });

        var index = 0;

        var modal = $('div#modal-chart');
        var modalAnchorLeft = modal.find('a.left');
        var modalAnchorRight = modal.find('a.right');

        function putAnchorDataToTheModal(anchor) {
            modal.find('div.modal-header span').html(anchor.data('symbol'));

            var img = $('<img alt="">');
            img.attr('src', anchor.attr('href'));
            modal.find('div.modal-body').html(img);

            var roiValues = {
                'roi_1m': anchor.data('roi-one-month'),
                'roi_3m': anchor.data('roi-three-months'),
                'roi_6m': anchor.data('roi-six-months'),
                'roi_1': anchor.data('roi-one'),
                'roi_2': anchor.data('roi-two'),
                'roi_3': anchor.data('roi-three'),
                'roi_best': anchor.data('roi-best'),
                'roi_worst': anchor.data('roi-worst')
            };
            var html = '<b>ROI1M:</b> ' + roiValues.roi_1m + '%, '
                + '<b>ROI3M:</b> ' + roiValues.roi_3m + '%, '
                + '<b>ROI6M:</b> ' + roiValues.roi_6m + '%, '
                + '<b>ROI1:</b> ' + roiValues.roi_1 + '% '
                + '<b>ROI2:</b> ' + roiValues.roi_2 + '% '
                + '<b>ROI3:</b> ' + roiValues.roi_3 + '%<br>'
                + '<b>ROI BEST:</b> ' + roiValues.roi_best + '% '
                + '<b>ROI WORST:</b> ' + roiValues.roi_worst + '% ';
            modal.find('div.modal-footer p').html(html);

            index = parseInt(anchor.data('index'));
            if (index == 0) {
                modalAnchorLeft.hide();
            } else if (!modalAnchorLeft.is(':visible')) {
                modalAnchorLeft.show();
            }
            if (index == maxIndex) {
                modalAnchorRight.hide();
            } else if (!modalAnchorRight.is(':visible')) {
                modalAnchorRight.show();
            }
        }

        $('a.anchor-chart-modal-opener').click(function (event) {
            event.preventDefault();
            putAnchorDataToTheModal($(this));
            modal.modal();
        });

        modal.find('div.modal-header a').click(function () {
            var self = $(this);
            var newIndex = 0;
            if (self.hasClass('left')) {
                newIndex = index - 1;
            } else if (self.hasClass('right')) {
                newIndex = index + 1;
            } else {
                console.error('No valid class attached to the anchor');
                return;
            }

            var anchor = $('a#chart-modal-opener-' + newIndex);
            if (!anchor.length) {
                console.error('Can not find anchor with index: ' + newIndex);
                return;
            }
            putAnchorDataToTheModal(anchor);
        });
    });
})(jQuery);
