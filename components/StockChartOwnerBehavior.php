<?php

namespace app\components;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use app\models\StockChart;

class StockChartOwnerBehavior extends Behavior 
{
    public $chartsListOwnerPropertyName;
    
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
        ];
    }
    
    public function afterFind()
    {
        $symbol = $this->owner->symbol;

        if (!isset($this->owner->{$this->chartsListOwnerPropertyName})) {
            $this->owner->{$this->chartsListOwnerPropertyName} = [];
        }
        $list = &$this->owner->{$this->chartsListOwnerPropertyName};
        
        foreach (StockChart::$types as $type) {
            $stockChart = new StockChart($symbol, $type);
            if (file_exists($stockChart->getChartFilePath())) {
                $list[$type] = $stockChart;
            }
        }
    }
    
}