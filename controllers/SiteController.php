<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use app\models\Stock;
use app\models\StockPrice;
use app\models\SearchForm;
use app\models\Industry;
use app\models\StockHistory;

/**
 * Class SiteController
 *
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get', 'post'],
                    'stock' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Get page title based on a search form params
     *
     * @param SearchForm $searchForm
     */
    private function getTitleBasedOnSearchForm(SearchForm $searchForm)
    {
        $title = $searchForm->roi_max ? Yii::t('app', 'Max') : Yii::t('app', 'Min');
    
        $map = [
            'roi_1m' => 'ROI1M',
            'roi_3m' => 'ROI3M',
            'roi_6m' => 'ROI6M',
            'roi_1' => 'ROI1',
            'roi_2' => 'ROI2',
            'roi_3' => 'ROI3',
        ];
        $items = [];
        foreach ($map as $property => $name) {
            if (!isset($searchForm->{$property})) {
                continue;
            }
            $value = (int)$searchForm->{$property};
            if ($value == 0) {
                continue;
            }
            $items[] = $name . ': ' . $value . '%';
        }

        return count($items) > 0 ? ($title .= ' ' . implode(' ', $items)) : null;
    }

    /**
     * Default action
     */
    public function actionIndex()
    {
        $searchForm = new SearchForm();
        $searchForm->load(Yii::$app->request->get());
        $searchForm->validate();   


        $dateTo = $searchForm->date_to ? new \DateTime($searchForm->date_to) : new \DateTime();
        $dateToMinusThreeYears = clone $dateTo;
        $dateToMinusThreeYears->add(\DateInterval::createFromDateString('-3 years'));

        // Get ROIs list and stock ids list
        $roiModels = $searchForm->dataProvider->getModels();
        $stockIds = [];
        foreach ($roiModels as $roiModel) {
            $stockIds[] = $roiModel->stock_id;
        }

        // Get stocks list
        $stockModels = Stock::find()
            ->where(['id' => $stockIds])
            ->all();
        unset($stockIds);    
        $industryIds = [];
        $prepared = [];
        foreach ($stockModels as $stockModel) {
            foreach ($roiModels as $roiModel) {
                if ($roiModel->stock_id == $stockModel->id) {
                    $prepared[$roiModel->id] = $stockModel;
                }                                
            }
            $industryIds[] = $stockModel->industry_id;
        }
        $stockModels = $prepared;
        unset($prepared);

        // Get industries list
        $industryIds = array_unique($industryIds);
        $industryModels = Industry::find()
            ->where(['id' => $industryIds])
            ->all();
        unset($industryIds);   
        $prepared = []; 
        foreach ($stockModels as $stockModel) {
            foreach ($industryModels as $industryModel) {
                if ($stockModel->industry_id == $industryModel->id) {
                    $prepared[$stockModel->id] = $industryModel;
                }
            }
        }
        $industryModels = $prepared;
        unset($prepared);

        return $this->render(
            'index', 
            [
                'title' => $this->getTitleBasedOnSearchForm($searchForm),
                'searchForm' => $searchForm, 
                'sortFieldName' => $this->getSortFieldName(),
                'sortPeriod' => $this->getSortPeriod(),
                'sortDirection' => $this->getSortDirection(),
                'dateTo' => $dateTo,
                'dateToMinusThreeYears' => $dateToMinusThreeYears,
                'showCharts' => $dateTo->diff(new \DateTime())->m == 0 || $searchForm->go_direction, 
                'stockModels' => $stockModels,
                'industryModels' => $industryModels,
                'roiPeriods' => [
                    'roi_1' => $this->getSortPeriod('roi_1'),
                    'roi_2' => $this->getSortPeriod('roi_2'),
                    'roi_3' => $this->getSortPeriod('roi_3'),
                    'roi_1m' => $this->getSortPeriod('roi_1m'),
                    'roi_3m' => $this->getSortPeriod('roi_3m'),
                    'roi_6m' => $this->getSortPeriod('roi_6m'),
                    'roi_3years' => $this->getSortPeriod('roi_3years'),
                ],
            ]
        );
    }

    public function actionStock()
    {

        $stockId = Yii::$app->request->get('stock_id', null);
        if (is_null($stockId)) {
            throw new BadRequestHttpException(Yii::t('app', 'Empty stock id'));
        }
        if (!is_numeric($stockId)) {
            throw new BadRequestHttpException(Yii::t('app', 'Stock id is not numeric'));
        }

        return $this->render(
            'stock',
            [
                'stockHistory' => new StockHistory($stockId),
            ]
        );
    }

    /**
     * 
     * @return string
     */
    private function getSortDirection()
    {
        $sort = Yii::$app->request->get('sort');
        return ($sort{0} === '-' ? 'desc' : 'asc');
    }

    /**
     * Get sort period
     *
     * @param string $sort
     * @return array|null
     */
    private function getSortPeriod($sort = null)
    {
        if ($sort === null) {
            $sort = Yii::$app->request->get('sort');
        }
        if (!$sort) {
            return null;
        }

        $field = str_replace('-', null, $sort);

        $beginDt = new \DateTime('first day of this month');
        $endDt = clone $beginDt;

        switch ($field) {
            case 'roi_1':
                $beginDt->add(\DateInterval::createFromDateString('-1 years'));
                break;
            case 'roi_2':
                $beginDt->add(\DateInterval::createFromDateString('-2 years'));
                $endDt->add(\DateInterval::createFromDateString('-1 years'));
                break;
            case 'roi_3':
                $beginDt->add(\DateInterval::createFromDateString('-3 years'));
                $endDt->add(\DateInterval::createFromDateString('-2 years'));
                break;
            case 'roi_1m':
                $beginDt->add(\DateInterval::createFromDateString('-1 months'));
                break;    
            case 'roi_3m':
                $beginDt->add(\DateInterval::createFromDateString('-3 months'));
                break;
            case 'roi_6m':
                $beginDt->add(\DateInterval::createFromDateString('-6 months'));
                break;
            case 'roi_3years':
                $beginDt->add(\DateInterval::createFromDateString('-3 years'));
                break;
            case 'eps':
                break;
            case 'roi_best':
            case 'roi_worst':
                $beginDt->add(\DateInterval::createFromDateString('-1 years'));
                break;
            default:
                throw new BadRequestHttpException('Unknown sort field: ' . $field);                 
        }

        return [$beginDt, $endDt];
    }

    /**
     * Get sort field name
     *
     * @return string|null
     */
    private function getSortFieldName()
    {
        $sort = Yii::$app->request->get('sort', null);
        if (!$sort) {
            return null;
        }
        $field = str_replace(['-', '_'], null, $sort);
        $field = str_replace('years', 'Y', $field);
        return strtoupper($field);
    }

}
