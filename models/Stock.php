<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use app\models\StockPrice;
use app\models\StockRoi;
use app\components\StockChartOwnerBehavior;

/**
 * This is the model class for table "stock".
 *
 * @property string $id
 * @property string $symbol_id
 * @property string $sector_id
 * @property string $industry_id
 * @property string $exchange_id
 * @property string $symbol
 * @property string $market_cap
 * @property integer $ipo_year
 * @property string $summary_quote
 * @property string $created_at
 * @property string $updated_at
 * @property string $stock_price
 * @property integet $volume
 *
 * @property Sector $sector
 * @property Industry $industry
 * @property Exchange $exchange
 * @property StockPrice[] $stockPrices
 */
class Stock extends \yii\db\ActiveRecord
{

    public $charts;
    private $lastRoiModel;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sector_id', 'industry_id', 'exchange_id', 'symbol', 'market_cap', 'created_at', 'updated_at'], 'required'],
            [['sector_id', 'industry_id', 'exchange_id', 'ipo_year'], 'integer'],
            [['market_cap', 'stock_price', 'volume'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['summary_quote'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sector_id' => Yii::t('app', 'Sector ID'),
            'industry_id' => Yii::t('app', 'Industry ID'),
            'exchange_id' => Yii::t('app', 'Exchange ID'),
            'symbol' => Yii::t('app', 'Symbol'),
            'market_cap' => Yii::t('app', 'Market Cap'),
            'ipo_year' => Yii::t('app', 'Ipo Year'),
            'summary_quote' => Yii::t('app', 'Summary Quote'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'stock_price' => Yii::t('app', 'Stock Price'),
            'volume' => Yii::t('app', 'Volume'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => StockChartOwnerBehavior::className(), 
                'chartsListOwnerPropertyName' => 'charts'
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSector()
    {
        return $this->hasOne(Sector::className(), ['id' => 'sector_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(Industry::className(), ['id' => 'industry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchange()
    {
        return $this->hasOne(Exchange::className(), ['id' => 'exchange_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockPrices()
    {
        return $this->hasMany(StockPrice::className(), ['stock_id' => 'id']);
    }

    /**
     * @return StockRoi
     */
    public function getLastRoi()
    {
        if (!is_null($this->lastRoiModel)) {
            return $this->lastRoiModel;
        }

        $lastRoiModel = StockRoi::find()
            ->where(['stock_id' => $this->id])
            ->orderBy('date DESC')
            ->one();

        if (!$lastRoiModel) {
            $lastRoiModel = new StockRoi();
        }    

        return $this->lastRoiModel = $lastRoiModel;
    }

    /**
     * Get average roi for the last 12 months
     */
    public function getAverageRoi()
    {
        $currentDt = new \DateTime();
        $yearAgoDt = clone $currentDt;
        $yearAgoDt->sub(\DateInterval::createFromDateString('+1 years'));

        $oneMonthInterval = \DateInterval::createFromDateString('+1 months');

        $roiSum = 0;
        $months = 0;
        $dates = [];
        do {
            $dates[] = $currentDt->format('Y-m-01');
            $currentDt->sub($oneMonthInterval);
        } while ($currentDt > $yearAgoDt);

        $stockRoiModels = StockRoi::find()
            ->where(['stock_id' => $this->id])
            ->andWhere(['date' => $dates])
            ->all();

        foreach ($stockRoiModels as $item) {
            $roiSum += $item->roi_1m;
            $months++;
        } 

        return $roiSum / $months;
    }

}
