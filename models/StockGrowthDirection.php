<?php

namespace app\models;

use Yii;
use yii\db\Expression as DbExpression;

/**
 * This is the model class for table "stock_growth_direction".
 *
 * @property string $id
 * @property string $stock_id
 * @property string $created_at
 * @property integer $growth_direction
 * @property string $from
 * @property integer $count_of_months
 *
 * @property Stock $stock
 */
class StockGrowthDirection extends \yii\db\ActiveRecord
{
    const GROWTH_DIRECTION_UP = 1;
    const GROWTH_DIRECTION_DOWN = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_growth_direction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stock_id', 'growth_direction', 'from'], 'required'],
            [['stock_id', 'growth_direction', 'count_of_months', 'count_of_quarters'], 'integer'],
            [['from'], 'date', 'format' => 'php:Y-m-d'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stock_id' => Yii::t('app', 'Stock ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'growth_direction' => Yii::t('app', 'Growth Direction'),
            'from' => Yii::t('app', 'From Date'),
            'count_of_months' => Yii::t('app', 'Count Of Months'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->isNewRecord) {
            $this->created_at = new DbExpression('NOW()');
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStock()
    {
        return $this->hasOne(Stock::className(), ['id' => 'stock_id']);
    }

    /**
     * @param array $set A set of values
     * @return boolean
     */
    public static function isSetGrowUp(array $set)
    {
        if (count($set) < 2) {
            return false;
        }
        $previousValue = 0;
        foreach ($set as $value) {
            if ($value > $previousValue) {
                $previousValue = $value;
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * @param array $set A set of values
     * @return boolean
     */
    public static function isSetGrowDown(array $set)
    {
        if (count($set) < 2) {
            return false;
        }
        $previousValue = max($set) + 1;
        foreach ($set as $value) {
            if ($value < $previousValue) {
                $previousValue = $value;
            } else {
                return false;
            }
        }
        return true;
    }
}
