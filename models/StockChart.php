<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use himiklab\thumbnail\EasyThumbnailImage;

/**
 * Class StockChart
 * 
 * @package app\models
 */
class StockChart
{
    /**
     * @var string
     */
    private $symbol;

    /**
     * @var int
     */
    private $type;

    /**
     * @var array
     * @static
     */
    public static $types = [
        'TYPE_DAILY' => 1, 'TYPE_WEEKLY' => 2, 'TYPE_YEARLY_3' => 3,
    ];

    /**
     * @var array
     */
    private static $urlTemplates;
    
    /**
     * 
     * @param string $symbol
     * @param int $type
     */
    public function __construct($symbol, $type)
    {
        $this->symbol = $symbol;
        $this->type = $type;

        if (is_null(self::$urlTemplates)) {
            self::$urlTemplates = [
                self::$types['TYPE_DAILY'] => 'http://elite.finviz.com/chart.ashx?t={symbol}&ty=c&ta=0&p=d&s=l',
                self::$types['TYPE_WEEKLY'] => 'http://elite.finviz.com/chart.ashx?t={symbol}&ty=c&ta=0&p=w&s=l',
                self::$types['TYPE_YEARLY_3'] => 'http://chart.finance.yahoo.com/z?s={symbol}&t=3y&q=l&l=off&z=l', 
            ];
        }
    }
    
    /**
     * 
     * @throws \DomainException
     */
    public function getSourceUrl()
    {
        $this->checkType($this->type);
        return str_replace('{symbol}', $this->symbol, self::$urlTemplates[$this->type]);
    }

    private function getChartFileNameRelatedToChartsDirectory()
    {
        return substr($this->symbol, 0, 1) . '/' . $this->symbol . '_' 
            . $this->convertChartTypeNumberToString($this->type) . '.jpg';   
    }
    
    /**
     * 
     * @throws \DomainException
     */
    public function getChartFileUrl()
    {
        $this->checkType($this->type);
        return Yii::getAlias('@web') . '/charts/' 
            . $this->getChartFileNameRelatedToChartsDirectory();
    }

    /**
     * 
     * @param int $width
     * @param int $height
     *
     * @return string
     */
    public function getChartThumbnailUrl($width, $height)
    {
        if (!is_numeric($width) || !is_numeric($height)) {
            throw new \InvalidArgumentException('Width or Height has not numeric value');
        }

        EasyThumbnailImage::$cacheAlias = 'charts/thumbs';
        return EasyThumbnailImage::thumbnailFileUrl(
            $this->getChartFilePath(), $width, $height
        );
    }
    
    /**
     *
     * @throws \DomainException
     */
    public function getChartFilePath()
    {
        $this->checkType($this->type);
        return self::getChartsDirectoryPath() . '/' 
            . $this->getChartFileNameRelatedToChartsDirectory();;
    }
    
    /**
     *
     * @return boolean
     * @throws \DomainException
     */
    public function shouldUpdate()
    {
        $path = $this->getChartFilePath();
        if (!file_exists($path)) {
            return true;
        }
        
        $lifetime = Yii::$app->params['chart_lifetime'];
        if (filectime($path) < time() - $lifetime) {
            return true;
        }
        
        return false;
    }
    
    /**
     *
     * @return string
     */
    public static function getChartsDirectoryPath()
    {
        $chartsDirectoryPath = Yii::getAlias('@webroot') . '/charts';
        if (!is_dir($chartsDirectoryPath) && !mkdir($chartsDirectoryPath)) {
            throw new Exception('Can not create directory "' . $chartsDirectoryPath . '"');
        }
        return $chartsDirectoryPath;        
    }
    
    /**
     * 
     * @return string
     */
    public static function getDummyChartFileUrl()
    {
        $path = self::getChartsDirectoryPath() . '/dummy.jpg';
        if (!file_exists($path)) {
            throw new Exception('A dummy chart image not found');
        } 
        return Yii::getAlias('@web') . '/charts/dummy.jpg';
    }

    /**
     * 
     * @throws \DomainException
     */
    private function checkType($type)
    {
        if (!in_array($type, self::$types)) {
            throw new \DomainException('Unknown type: ' . $type);
        }
    }

    /**
     * 
     * @param int $type
     *
     * @return string
     */
    private function convertChartTypeNumberToString($type)
    {
        return strtolower(array_keys(self::$types, $type)[0]);
    }
}