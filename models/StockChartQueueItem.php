<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stock_chart_queue_item".
 *
 * @property string $id
 * @property string $symbol
 * @property string $url
 */
class StockChartQueueItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_chart_queue_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['symbol', 'url'], 'required'],
            [['symbol'], 'string', 'max' => 32],
            [['url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'symbol' => Yii::t('app', 'Symbol'),
            'url' => Yii::t('app', 'Url'),
        ];
    }
}
