<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * The model to get stock history through data provider
 */
class StockHistory extends Model
{
	const DATE_PROVIDER_PAGE_SIZE = 50;

	/**
	 * @var int
	 */
	private $stockId;

	/**
	 * @var ActiveDataProvider
	 */
	private $dataProvider;

	/**
	 * @var Stock
	 */
	private $stock;

	/**
	 * 
	 * @param int $stockId
	 *
	 * @throws \InvalidArgumentException
	 */
	public function __construct($stockId)
	{
		if (!is_numeric($stockId)) {
			throw new \InvalidArgumentException('The stock id is not numeric');
		}
		$this->stockId = $stockId;
	}

	/**
	 *
	 * @return Stock
	 */
	public function getStock()
	{
		if ($this->stock) {
			return $this->stock;
		}
		return $this->stock = Stock::findOne($this->stockId);
	}

	/**
	 *
	 * @return ActiveDataProvider
	 */
	public function getDataProvider()
	{
		if ($this->dataProvider) {
			return $this->dataProvider;
		}

		$date = date('Y-m-01');
		$query = (new Query())
			->select(['stock_roi.date AS date', 'stock_price.open AS price', 'stock_roi.roi_1m AS roi_1m'])
			->from('stock_roi')
			->leftJoin('stock_price', 'stock_price.stock_id = stock_roi.stock_id AND stock_price.date = stock_roi.date')
			->where(['<=', 'stock_roi.date', $date])
			->andWhere(['stock_roi.stock_id' => $this->stockId]);
			
		$this->dataProvider = new ActiveDataProvider(
			[
				'query' => $query,
				'pagination' => [
					'pageSize' => self::DATE_PROVIDER_PAGE_SIZE,
				],
				'sort' => [
					'attributes' => [
						'date' => [
							'asc' => ['date' => SORT_ASC],
							'desc' => ['date' => SORT_DESC],
							'default' => SORT_DESC,
							'label' => Yii::t('app', 'Date'),
						],
						'price' => [
							'asc' => ['price' => SORT_ASC],
							'desc' => ['price' => SORT_DESC],
							'default' => SORT_ASC,
							'label' => Yii::t('app', 'Price'),
						],
						'roi_1m' => [
							'asc' => ['roi_1m' => SORT_ASC],
							'desc' => ['roi_1m' => SORT_DESC],
							'default' => SORT_ASC,
							'label' => Yii::t('app', 'ROI1M'),
						],
					],
					'defaultOrder' => [
						'date' => SORT_DESC,
					],
				],
			]
		);

		return $this->dataProvider;
	}
} 