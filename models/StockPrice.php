<?php

namespace app\models;

use Yii;
use app\models\Stock;

/**
 * This is the model class for table "stock_price".
 *
 * @property string $id
 * @property string $stock_id
 * @property string $date
 * @property string $open
 * @property string $high
 * @property string $low
 * @property string $close
 * @property string $volume
 * @property string $adj_close
 *
 * @property Stock $stock
 */
class StockPrice extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    private static $stockPriceModelToStockList = [];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stock_id', 'date', 'open', 'high', 'low', 'close', 'volume', 'adj_close', 'symbol'], 'required'],
            [['stock_id'], 'integer'],
            [['date'], 'safe'],
            // [['symbol'], 'match', 'pattern' => '[A-Z0-9]+'],
            [['open', 'high', 'low', 'close', 'volume', 'adj_close'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stock_id' => Yii::t('app', 'Stock ID'),
            'date' => Yii::t('app', 'Date'),
            'open' => Yii::t('app', 'Open'),
            'high' => Yii::t('app', 'High'),
            'low' => Yii::t('app', 'Low'),
            'close' => Yii::t('app', 'Close'),
            'volume' => Yii::t('app', 'Volume'),
            'adj_close' => Yii::t('app', 'Adj Close'),
            'symbol' => Yii::t('app', 'Symbol'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStock()
    {
        return $this->hasOne(Stock::className(), ['id' => 'stock_id']);
    }


    /**
     * @param array $stockIds
     * @param \DateTime $dt
     * @param int $countOfYears
     *
     * @return array
     */
    public static function loadModelsForNYearsToAMemory(array $stockIds, \DateTime $dt, $countOfYears = 3)
    {

        $firstDateOfMonth = $dt->format('Y-m-01');

        $firstDateOfMonthNYearsAgo = $dt->add(\DateInterval::createFromDateString('-' . $countOfYears . ' years'))
            ->format('Y-m-01');
        
        $result = static::find()
            ->where(['<=', 'date', $firstDateOfMonth])
            ->andWhere(['>=', 'date', $firstDateOfMonthNYearsAgo])
            ->andWhere(['stock_id' => $stockIds])
            ->all();

        foreach ($result as $item) {
            
            $dt = new \DateTime($item->date);
            $firstDateOfMonth = $dt->format('Y-m-01');

            if (!isset(self::$stockPriceModelToStockList[$firstDateOfMonth])) {
                self::$stockPriceModelToStockList[$firstDateOfMonth] = [];
            }

            self::$stockPriceModelToStockList[$firstDateOfMonth][$item->stock_id] = $item;
        }


        return self::$stockPriceModelToStockList;
    }

    private static function getFirstDateOfMonth(\DateTime $dt, \DateInterval $interval = null)
    {
        if ($interval instanceof \DateInterval) {
            $dt->add($interval);
        }
        return $dt->format('Y-m-01');
    }

    /**
     * Gets stock price model by stock and date time with optionally added interval
     *
     * @param Stock $stock
     * @param \DateTime $dt
     *
     * @return StockPrice
     */
    public static function getStockPriceModelByStockAndDateTime(Stock $stock, \DateTime $dt, \DateInterval $interval = null)
    {

        $firstDateOfMonth = self::getFirstDateOfMonth($dt, $interval);
        $list = self::$stockPriceModelToStockList;
        if (!isset($list[$firstDateOfMonth][$stock->id])) {
            return null;
        }   

        return $list[$firstDateOfMonth][$stock->id];
    }

    public static function getStockPriceModelByStockAndDateTimeLive(Stock $stock, \DateTime $dt, \DateInterval $interval = null)
    {
        $firstDateOfMonth = self::getFirstDateOfMonth($dt, $interval);
        $model = StockPrice::find()
            ->where(['stock_id' => $stock->id])
            ->andWhere(['date' => $firstDateOfMonth])
            ->one();
        return $model;    
    }
}
