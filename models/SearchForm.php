<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\base\Model;
use yii\db\Query;
use yii\web\BadRequestHttpException;
use app\models\StockRoi;
use app\models\StockPrice;
use app\models\StockGrowthDirection;

class SearchForm extends Model
{
    const EPS_DIRECTION_LESSER = 1;
    const EPS_DIRECTION_GREATER = 2;

    private $dataProvider;
    public $symbol;
    public $stock_price;
    public $market_cap;
    public $volume;
	public $roi_1;
	public $roi_2;
	public $roi_3;
    public $roi_1m;
	public $roi_3m;
    public $roi_6m;
	public $roi_3years;
    public $roi_best_min;
    public $roi_best_max;
    public $roi_worst_min;
    public $roi_worst_max;
    public $roi_max = false;
    public $date_to = null;
    public $go_direction;
    public $count_of_months_to_go;
    public $count_of_quarters_to_go;
    public $eps;
    public $eps_direction;

	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['symbol'], 'match', 'pattern' => '/[A-Z0-9]+/'],
            [
                [
                    'stock_price', 
                    'market_cap', 
                    'volume',
                    'roi_1', 
                    'roi_2', 
                    'roi_3', 
                    'roi_1m',
                    'roi_3m',
                    'roi_6m', 
                    'roi_3years',
                    'roi_best_min',
                    'roi_best_max',
                    'roi_worst_min',
                    'roi_worst_max',
                    'eps',
                ], 
                'number'
            ],
            [
                ['go_direction'], 'in', 
                'range' => [StockGrowthDirection::GROWTH_DIRECTION_UP, StockGrowthDirection::GROWTH_DIRECTION_DOWN]
            ],
            [
                ['eps_direction'], 'in',
                'range' => [self::EPS_DIRECTION_LESSER, self::EPS_DIRECTION_GREATER]
            ],
            [['count_of_months_to_go'], 'in', 'range' => range(1, 12)],
            [['count_of_quarters_to_go'], 'in', 'range' => range(1, 12)],
            [['roi_max'], 'boolean'],
            [['date_to'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'stock_price' => Yii::t('app', 'Min Stock Price'),
            'market_cap' => Yii::t('app', 'Min Capitalisation'),
            'volume' => Yii::t('app', 'Min Volume'),
            'roi_1' => Yii::t('app', 'ROI1'),
            'roi_2' => Yii::t('app', 'ROI2'),
            'roi_3' => Yii::t('app', 'ROI3'),
            'roi_1m' => Yii::t('app', 'ROI1M'),
            'roi_3m' => Yii::t('app', 'ROI3M'),
            'roi_6m' => Yii::t('app', 'ROI6M'),
            'roi_3years' => Yii::t('app', 'ROI3Y'),
            'roi_max' => Yii::t('app', 'Max ROI'),
            'date_to' => Yii::t('app', 'Date'),
            'go_direction' => Yii::t('app', 'Go'),
            'count_of_months_to_go' => Yii::t('app', 'Count Of Months to Go'),
            'count_of_quarters_to_go' => Yii::t('app', 'Count of Quarters to Go'),
            'eps_direction' => Yii::t('app', 'EPS Dir.'),
            'eps' => Yii::t('app', 'EPS Val.'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();
        if (!$this->date_to) {
            $this->date_to = null;
        } else {
            $dt = new \DateTime($this->date_to);
            $currentDt = new \DateTime('first day of this months');
            if ($dt > $currentDt) {
                $this->date_to = $currentDt->format('Y-m-01');
            }
        }
    }

    /**
     *
     * @return ActiveDataProvider
     */
    public function getDataProvider()
    {
        if ($this->dataProvider) {
            return $this->dataProvider;
        }

    	$query = StockRoi::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => 200
                ],
                'sort' => [
                    'defaultOrder' => [
                        'roi_1' => SORT_ASC
                    ],
                ]
            ]
        );

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Add ROI parameters to the query
        $names = ['roi_1', 'roi_2', 'roi_3', 'roi_1m', 'roi_3m', 'roi_6m', 'roi_3years'];
        $operator = $this->roi_max ? '<=' : '>=';
        foreach ($names as $propertymName) {
            if (!$this->$propertymName) {
                continue;
            }
            $query->andWhere([$operator, $propertymName, $this->$propertymName]);
        }

        // Add date to the query
        $dt = $this->date_to ? new \DateTime($this->date_to) : new \DateTime();
        $query->andWhere(['date' => $dt->format('Y-m-01')]);

        // If "EPS direction" is specified then add it to the query
        if ($this->eps_direction) {
            $operator = $this->eps_direction == self::EPS_DIRECTION_LESSER ? '<' : '>';
            $query->andWhere([$operator, 'eps', (float)$this->eps]);
        }

        // If BestROI (max/min) is specified then add it to the query
        if ($this->roi_best_min) {
            $query->andWhere(['>=', 'roi_best', $this->roi_best_min]);
        }
        if ($this->roi_best_max) {
            $query->andWhere(['<=', 'roi_best', $this->roi_best_max]);
        }

        // If WorstROI (max/min) is specified then add it to the query
        if ($this->roi_worst_min) {
            $query->andWhere(['>=', 'roi_worst', $this->roi_worst_min]);
        }
        if ($this->roi_worst_max) {
            $query->andWhere(['<=', 'roi_worst', $this->roi_worst_max]);
        }

        // Join with stock
        $query->joinWith(
            [
                'stock' => function ($query) use ($dt) {
                    // Add common params
                    $names = ['stock_price', 'market_cap', 'volume'];
                    foreach ($names as $propertymName) {
                        if (!$this->$propertymName) {
                            continue;
                        }
                        $query->andWhere(['>=', $propertymName, $this->$propertymName]);
                    }
                    // Add symbol if specified
                    if ($this->symbol) {
                        $query->andWhere(['like', 'symbol', $this->symbol]);
                    }
                    // If "Go Direction" is specified then add it to the query
                    if ($this->go_direction) {
                        // Select stock ids which has a valid growth
                        $queryToStockGrowthDirection = (new Query())
                            ->select('stock_id')
                            ->from('stock_growth_direction')
                            ->where(['growth_direction' => $this->go_direction])
                            ->andWhere(['from' => $dt->format('Y-m-01')]);

                        $countAddedToTheQuery = false;
                        // If count of months is specified then use its value
                        if ($this->count_of_months_to_go) {
                            $queryToStockGrowthDirection->andWhere(['count_of_months' => $this->count_of_months_to_go]);
                            $countAddedToTheQuery = true;
                        } else if ($this->count_of_quarters_to_go) {
                            // else if count of quarters is specified then use its value
                            $queryToStockGrowthDirection->andWhere(['count_of_quarters' => $this->count_of_quarters_to_go]);
                            $countAddedToTheQuery = true;
                        }
                        // Continue only if count of months/quarters added to the query
                        if ($countAddedToTheQuery) {
                            // Get list of stock ids
                            $result = $queryToStockGrowthDirection->all();
                            $stockIds = [];
                            foreach ($result as $item) {
                                $stockIds[] = $item['stock_id'];
                            }    
                            // Select stocks with ids from the list
                            $query->andWhere(['stock.id' => $stockIds]);
                        }
                    }
                }
            ]
        );

        return $this->dataProvider = $dataProvider;
    }
}