<?php

namespace app\models;

use Yii;
use app\models\Stock;
use app\models\StockPrice;

/**
 * This is the model class for table "stock_roi".
 *
 * @property string $id
 * @property string $stock_id
 * @property string $date
 * @property string $roi_1
 * @property string $roi_2
 * @property string $roi_3
 * @property string $roi_1m
 * @property string $roi_3m
 * @property string $roi_6m
 * @property string $roi_3years
 * @property string $roi_best
 * @property string $roi_worst
 * @property float eps
 *
 * @property Stock $stock
 */
class StockRoi extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    private $roiSet;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_roi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stock_id', 'date'], 'required'],
            [['stock_id'], 'integer'],
            [['date'], 'date', 'format' => 'php:Y-m-d'],
            [
                ['roi_1', 'roi_2', 'roi_3', 'roi_1m', 'roi_3m', 'roi_6m', 'roi_3years', 'roi_best', 'roi_worst', 'eps'], 
                'number'
            ],
            [['stock_id', 'date'], 'unique', 'targetAttribute' => ['stock_id', 'date'], 'message' => 'The combination of Stock ID and Date has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stock_id' => Yii::t('app', 'Stock ID'),
            'date' => Yii::t('app', 'Date'),
            'roi_1' => Yii::t('app', 'Roi 1'),
            'roi_2' => Yii::t('app', 'Roi 2'),
            'roi_3' => Yii::t('app', 'Roi 3'),
            'roi_1m' => Yii::t('app', 'Roi 1m'),
            'roi_3m' => Yii::t('app', 'Roi 3m'),
            'roi_6m' => Yii::t('app', 'Roi 6m'),
            'roi_3years' => Yii::t('app', 'Roi 3years'),
            'roi_best' => Yii::t('app', 'Best Roi'),
            'roi_worst' => Yii::t('app', 'Worst Roi'),
            'eps' => Yii::t('app', 'EPS'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStock()
    {
        return $this->hasOne(Stock::className(), ['id' => 'stock_id']);
    }

    /**
     * @inheritdoc
     */
    public function afterValidate() {
        
        parent::afterValidate();
        if ($this->hasErrors()) {
            return;
        }

        $stock = Stock::findOne($this->stock_id);
        $dt = new \DateTime($this->date);
        $this->calculateROI1($stock, clone $dt);
        $this->calculateROI2($stock, clone $dt);
        $this->calculateROI3($stock, clone $dt);
        $this->calculateROI1m($stock, clone $dt);
        $this->calculateROI3m($stock, clone $dt);
        $this->calculateROI6m($stock, clone $dt);
        $this->calculateROI3years($stock, clone $dt);
        $this->calculateROIBest($stock, clone $dt);
        $this->calculateROIWorst($stock, clone $dt);
    }

    /**
     * @param float $dividend
     * @param float $divider
     * @return float
     */
    private function calculateROI($dividend, $divider)
    {
        return round($dividend / $divider * 100, 4);
    }

    /**
     * @param Stock $stock
     * @param \DateTime $dt
     */
    public function calculateROI1(Stock $stock, \DateTime $dt)
    {
        /** @var StockPrice $currentStockPriceModel */
        $currentStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive($stock, clone $dt);
        if (!$currentStockPriceModel) {
            return;
        }

        /** @var StockPrice $oneYearAgoStockPriceModel */
        $oneYearAgoStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock,
            \DateTime::createFromFormat('Y-m-d', $currentStockPriceModel->date), 
            \DateInterval::createFromDateString('-1 years')
        );
        if (!$oneYearAgoStockPriceModel) {
            return;
        }

        $this->roi_1 = $this->calculateROI($currentStockPriceModel->open, $oneYearAgoStockPriceModel->open);
    }

    /**
     * @param Stock $stock
     * @param \DateTime $dt
     */
    protected function calculateROI2(Stock $stock, \DateTime $dt)
    {
        /** @var StockPrice $oneYearAgoStockPriceModel */
        $oneYearAgoStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock, 
            clone $dt, 
            \DateInterval::createFromDateString('-1 years')
        );
        if (!$oneYearAgoStockPriceModel) {
            return;
        }

        /** @var StockPrice $twoYearsAgoStockPriceModel */
        $twoYearsAgoStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock,
            \DateTime::createFromFormat('Y-m-d', $oneYearAgoStockPriceModel->date), 
            \DateInterval::createFromDateString('-1 years')
        );
        if (!$twoYearsAgoStockPriceModel) {
            return;
        }

        $this->roi_2 = $this->calculateROI($oneYearAgoStockPriceModel->open, $twoYearsAgoStockPriceModel->open);
    }

    /**
     * Calculates ROI3 for a Stock
     *
     * @param Stock $stock
     * @param \DateTime $dt
     */
    protected function calculateROI3(Stock $stock, \DateTime $dt)
    {
        /** @var StockPrice $twoYearsAgoStockPriceModel */
        $twoYearsAgoStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock,
            clone $dt, 
            \DateInterval::createFromDateString('-2 years')
        );
        if (!$twoYearsAgoStockPriceModel) {
            return;
        }

        /** @var StockPrice $threeYearsAgoStockPriceModel */
        $threeYearsAgoStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive( 
            $stock,
            \DateTime::createFromFormat('Y-m-d', $twoYearsAgoStockPriceModel->date), 
            \DateInterval::createFromDateString('-1 years')
        );
        if (!$threeYearsAgoStockPriceModel) {
            return;
        }

        $this->roi_3 = $this->calculateROI($twoYearsAgoStockPriceModel->open, $threeYearsAgoStockPriceModel->open);        
    }

    /**
     * @param Stock $stock
     * @param \DateTime $dt
     * @param boolean $assignValue
     * @return flaot|null
     */
    protected function calculateROI1m(Stock $stock, \DateTime $dt, $assignValue = true)
    {
        
        /** @var StockPrice $currentMonthStockPriceModel */
        $currentMonthStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock,
            clone $dt
        );
        if (!$currentMonthStockPriceModel) {
            return null;
        }
        /** @var StockPrice $oneMonthAgoStockPriceModel */
        $oneMonthAgoStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock,
            \DateTime::createFromFormat('Y-m-d', $currentMonthStockPriceModel->date),  
            \DateInterval::createFromDateString('-1 months')
        );

        if (!$oneMonthAgoStockPriceModel) {
            return null;
        }

        $value = $this->calculateROI($currentMonthStockPriceModel->open, $oneMonthAgoStockPriceModel->open);
        if ($assignValue) {
            $this->roi_1m = $value;
        }
        
        return $value;
    }

    /**
     * @param Stock $stock
     * @param \DateTime $dt
     */
    protected function calculateROI3m(Stock $stock, \DateTime $dt)
    {
        /** @var StockPrice $currentMonthStockPriceModel */
        $currentMonthStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock, 
            clone $dt
        );
        if (!$currentMonthStockPriceModel) {
            return;
        }

        /** @var StockPrice $threeMonthsAgoStockPriceModel */
        $threeMonthsAgoStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock,
            \DateTime::createFromFormat('Y-m-d', $currentMonthStockPriceModel->date),  
            \DateInterval::createFromDateString('-3 months')
        );
        if (!$threeMonthsAgoStockPriceModel) {
            return;
        }

        $this->roi_3m = $this->calculateROI($currentMonthStockPriceModel->open, $threeMonthsAgoStockPriceModel->open);
    }

    /**
     * @param Stock $stock
     * @param \DateTime $dt
     */
    protected function calculateROI6m(Stock $stock, \DateTime $dt)
    {
        /** @var StockPrice $currentMonthStockPriceModel */
        $currentMonthStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock,
            clone $dt
        );
        if (!$currentMonthStockPriceModel) {
            return;
        }

        /** @var StockPrice $sixMonthsAgoStockPriceModel */
        $sixMonthsAgoStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock,
            \DateTime::createFromFormat('Y-m-d', $currentMonthStockPriceModel->date),  
            \DateInterval::createFromDateString('-6 months')
        );
        if (!$sixMonthsAgoStockPriceModel) {
            return;
        }

        $this->roi_6m = $this->calculateROI($currentMonthStockPriceModel->open, $sixMonthsAgoStockPriceModel->open);
    }

    /**
     * @param Stock $stock
     * @param \DateTime $dt
     */
    protected function calculateROI3years(Stock $stock, \DateTime $dt)
    {
        /** @var StockPrice $currentMonthStockPriceModel */
        $currentMonthStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock,
            clone $dt
        );
        if (!$currentMonthStockPriceModel) {
            return;
        }

        /** @var StockPrice $threeYearsAgoStockPriceModel */
        $threeYearsAgoStockPriceModel = StockPrice::getStockPriceModelByStockAndDateTimeLive(
            $stock,
            \DateTime::createFromFormat('Y-m-d', $currentMonthStockPriceModel->date),
            \DateInterval::createFromDateString('-3 years')
        );
        if (!$threeYearsAgoStockPriceModel) {
            return;
        }

        $this->roi_3years = $this->calculateROI($currentMonthStockPriceModel->open, $threeYearsAgoStockPriceModel->open);
    }

    /**
     * @param Stock $stock
     * @param \DateTime $dt
     */
    private function getRoiSet(Stock $stock, \DateTime $dt)
    {
        if (!$this->roiSet) {
            $roiSet = [];  

            $yearAgoDt = clone $dt;
            $yearAgoDt->sub(\DateInterval::createFromDateString('+1 years'));
            $currentIterDt = clone $dt;
            $oneMonthInterval = \DateInterval::createFromDateString('+1 months');

            do {
                $value = $this->calculateROI1m($stock, $currentIterDt, false);
                if ($value) {
                    $roiSet[] = $value;
                }
                $currentIterDt->sub($oneMonthInterval);
            } while ($currentIterDt > $yearAgoDt);

            return $this->roiSet = $roiSet;
        } else {
            return $this->roiSet;
        }
    }

    /**
     * @param Stock $stock
     * @param \DateTime $dt
     */
    protected function calculateROIBest(Stock $stock, \DateTime $dt)
    {
        $roiSet = $this->getRoiSet($stock, $dt);
        if (!$roiSet || count($roiSet) == 1) {
            return;
        }

        $this->roi_best = max($roiSet);
    }

    /**
     * @param Stock $stock
     * @param \DateTime $dt
     */
    protected function calculateROIWorst(Stock $stock, \DateTime $dt)
    {
        $roiSet = $this->getRoiSet($stock, $dt);
        if (!$roiSet || count($roiSet) == 1) {
            return;
        }

        $this->roi_worst = min($roiSet);
    }
}
